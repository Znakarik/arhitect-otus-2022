import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SolveSquareExpressionTest {

    private SolveSquareExpression underTest;

    @BeforeAll
    public void before() {
        underTest = new SolveSquareExpression();
    }

    /**
     * Для уравнения x^2+1 = 0 корней нет
     * (возвращается пустой массив)
     */
    @Test
    public void emptySquare() {
        double[] result = underTest.solve(1d, 0d, 1d);
        Assertions.assertEquals(0, result.length);
    }

    /**
     * Для уравнения x^2-1 = 0 есть два корня кратности 1 (x1=1, x2=-1)
     */
    @Test
    public void hasTwoSquare() {
        double[] result = underTest.solve(1d, 0d,-1d);
        Assertions.assertEquals(2, result.length);
        Arrays.sort(result);
        Assertions.assertEquals(-1, result[0], Constants.EPS);
        Assertions.assertEquals(1, result[1], Constants.EPS);
    }

    /**
     * Для уравнения x^2+2x+1 = 0 есть один корень кратности 2 (x1= x2 = -1).
     */
    @Test
    public void hasOneSquare() {
        double[] result = underTest.solve(1d,2d, 1d);
        Assertions.assertEquals(1, result.length);
        Assertions.assertEquals(-1, result[0], Constants.EPS);
    }

    /**
     * Коэффициент a не может быть равен 0
     */
    @Test
    public void zeroAThrowsEx() {
        UnsupportedOperationException ex = Assertions.assertThrows(
                UnsupportedOperationException.class,
                () -> underTest.solve(0d, 1d, 2d));
        Assertions.assertEquals("Коэффициент a не может быть == 0", ex.getMessage());
    }

    @Test
    public void nullCoefficient() {
        UnsupportedOperationException ex = Assertions.assertThrows(
                UnsupportedOperationException.class,
                () -> underTest.solve(null, 2d, 3d));
        Assertions.assertEquals("Коэффициенты могут быть только числовыми", ex.getMessage());
    }

    @Test
    public void notANumberCoefficients() {
        UnsupportedOperationException ex = Assertions.assertThrows(
                UnsupportedOperationException.class,
                () -> underTest.solve("123", 2d, 3d));
        Assertions.assertEquals("Коэффициенты могут быть только числовыми", ex.getMessage());
    }

    @Test
    public void nanCoefficient() {
        UnsupportedOperationException ex = Assertions.assertThrows(
                UnsupportedOperationException.class,
                () -> underTest.solve(Double.NaN, 2d, 3d));
        Assertions.assertEquals("Коэффициенты могут быть только числовыми", ex.getMessage());
    }

    @Test
    public void infinityCoefficient() {
        UnsupportedOperationException ex = Assertions.assertThrows(
                UnsupportedOperationException.class,
                () -> underTest.solve(3d, Double.NEGATIVE_INFINITY, 3d));
        Assertions.assertEquals("Коэффициенты могут быть только числовыми", ex.getMessage());
    }
}
