import java.util.Arrays;
import java.util.Objects;

public class SolveSquareExpression {

    public double[] solve(Object a, Object b, Object c) {
        if (!isValidNumbers(a, b, c)) {
            throw new UnsupportedOperationException("Коэффициенты могут быть только числовыми");
        }
        double A = (double) a;
        double B = (double) b;
        double C = (double) c;
        if (isZero(A)) {
            throw new UnsupportedOperationException("Коэффициент a не может быть == 0");
        }
        double D = (B * B) - 4 * A * C;
        if (isZero(D)) {
            return new double[]{-B / 2 * A};
        }
        if (D < Constants.EPS) {
            return new double[]{};
        }
        if (D > Constants.EPS) {
            return new double[]{B + Math.sqrt(D) / 2 * A, -B - Math.sqrt(D) / 2 * A};
        } else return null;
    }

    private boolean isZero(double num) {
        return Math.abs(num) < Constants.EPS;
    }

    private boolean isValidNumbers(Object... objects) {
        boolean containsNull = Arrays.stream(objects).anyMatch(Objects::isNull);
        if (containsNull) return false;
        boolean isAllNumbers = Arrays.stream(objects).allMatch(o -> o instanceof Number);
        boolean notContainsNan = Arrays.stream(objects).noneMatch(o -> o.equals(Double.NaN));
        boolean notContainsInfinity = Arrays.stream(objects).noneMatch(o -> o.equals(Double.NEGATIVE_INFINITY) || o.equals(Double.POSITIVE_INFINITY));
        return isAllNumbers && notContainsNan && notContainsInfinity;
    }
}
