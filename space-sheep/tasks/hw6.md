Предположим, что у нас есть интерфейс

```
interface Tank.Operations.IMovable
{
Vector getPosition();
Vector setPosition(Vector newValue);
Vector getVelocity();
}
```

1. Необходимо по нему сгенерировать следующий класс так, 
чтобы можно было создавать экземпляры этого класса: 

```java
class Tank implements IMovable {
    UObject obj;

    public Tank (UObject obj) {
        this.obj = obj;
    }

    public Vector getPosition() {
        return IoC.Resolve("Tank.Operations.IMovable:position.get", obj);
    }

    public Vector getVelocity() {
        return IoC.Resolve("Tank.Operations.IMovable:velocity.get", obj);
    }

    public Vector setPosition(Vector newValue) {
        return IoC.Resolve("Tank.Operations.IMovable:position.set", obj, newValue);
    }
 }
```


2. Экземпляры генерируемых оберток для объекта obj можно создавать следующей строкой:
```var adapter = IoC.Resolve("Adapter", typeof(IMovable), obj);```

3. Необязательная задача на подумать: если интерфейсе потребуются какие-либо методы, например, как
interface Tank.Operations.IMovable

```
    {
        Vector getPosition();
        Vector setPosition(Vector newValue);
        Vector getVelocity();
        void finish();
    }
```

Придумать реализацию адаптера для подобных методов.