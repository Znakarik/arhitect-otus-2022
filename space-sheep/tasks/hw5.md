
- [X] Разрешает зависимости с помощью метода, со следующей сигнатурой:
T IoC.Resolve(string key, params object[] args);
- [X] Регистрация зависимостей также происходит с помощью метода Resolve
IoC.Resolve("IoC.Register", "aaa", (args) => new A()).Execute();
- [X] Зависимости можно регистрировать в разных "скоупах"
IoC.Resolve("Scopes.New", "scopeId").Execute();
IoC.Resolve("Scopes.Current", "scopeId").Exceute();
- [X] Указание: Для работы со скоупами используйте ThreadLocal контейнер.