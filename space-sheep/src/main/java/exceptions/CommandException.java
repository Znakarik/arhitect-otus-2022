package exceptions;

public class CommandException extends RuntimeException {

    public CommandException(Throwable cause) {
        super(cause);
    }
}
