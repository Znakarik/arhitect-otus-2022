package exceptions;

public class RotateCommandException extends RuntimeException {

    public RotateCommandException(Throwable cause) {
        super(cause);
    }
}
