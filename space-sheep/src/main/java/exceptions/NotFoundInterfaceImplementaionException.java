package exceptions;

public class NotFoundInterfaceImplementaionException extends RuntimeException {
    public NotFoundInterfaceImplementaionException(String message) {
        super(message);
    }
}
