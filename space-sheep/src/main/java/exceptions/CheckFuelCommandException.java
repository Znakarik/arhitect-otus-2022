package exceptions;

public class CheckFuelCommandException extends RuntimeException {

    public CheckFuelCommandException(String message) {
        super(message);
    }

    public CheckFuelCommandException() {
        super();
    }

    public CheckFuelCommandException(Throwable cause) {
        super(cause);
    }
}
