package exceptions;

public class ChangeVelocityCommandException extends RuntimeException {
    public ChangeVelocityCommandException(Throwable cause) {
        super(cause);
    }
}
