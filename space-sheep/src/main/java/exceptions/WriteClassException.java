package exceptions;

public class WriteClassException extends RuntimeException {
    public WriteClassException(Throwable cause) {
        super(cause);
    }
}
