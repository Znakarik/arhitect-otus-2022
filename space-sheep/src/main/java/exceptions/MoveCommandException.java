package exceptions;

public class MoveCommandException extends RuntimeException {
    public MoveCommandException(Throwable cause) {
        super(cause);
    }
}
