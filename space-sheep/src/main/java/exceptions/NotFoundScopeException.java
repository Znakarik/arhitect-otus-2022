package exceptions;

public class NotFoundScopeException extends RuntimeException {
    public NotFoundScopeException(String message) {
        super(message);
    }
}
