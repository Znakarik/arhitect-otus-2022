package exceptions.handlers;

import command.Command;

import ioc.Ioc;

public class TryToRepeatIfThrowsLogHandler implements Command, ExceptionHandler {

    private Command command;
    private int repeatAmount;

    public TryToRepeatIfThrowsLogHandler(Command command, int repeatAmount) {
        this.command = command;
        this.repeatAmount = repeatAmount;
    }

    @Override
    public void execute() {
        do {
            try {
                command.execute();
            } catch (Exception e) {
                if (repeatAmount == 1) {
                    Command logCommand = Ioc.Resolve("LogCommand", e);
                    logCommand.execute();
                }
            } finally {
                repeatAmount--;
            }
        } while (repeatAmount > 0);
    }
}
