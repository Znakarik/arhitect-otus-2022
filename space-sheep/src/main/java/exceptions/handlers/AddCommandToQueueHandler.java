package exceptions.handlers;

import command.Command;
import exceptions.CommandException;
import ioc.Ioc;

import java.util.Queue;

public class AddCommandToQueueHandler implements Command, ExceptionHandler {

    private Command command;

    public AddCommandToQueueHandler(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        Queue queue = null;
        try {
            queue = Ioc.Resolve("Commands.Queue");
        } catch (Throwable throwable) {
            new CommandException(throwable);
        }
        queue.add(command);
    }
}
