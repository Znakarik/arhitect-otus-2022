package exceptions;

public class CreateClassException extends RuntimeException {

    public CreateClassException(Throwable cause) {
        super(cause);
    }

    public CreateClassException(String message, Throwable cause) {
        super(message, cause);
    }
}
