package exceptions;

public class BurnFuelCommandException extends RuntimeException {
    public BurnFuelCommandException(Throwable cause) {
        super(cause);
    }
}
