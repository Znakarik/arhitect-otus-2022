package exceptions;

public class CouldNotInstantiateClass extends RuntimeException {
    public CouldNotInstantiateClass(String message) {
        super(message);
    }
}
