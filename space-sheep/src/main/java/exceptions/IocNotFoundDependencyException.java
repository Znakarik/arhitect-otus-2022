package exceptions;

public class IocNotFoundDependencyException extends RuntimeException {

    public IocNotFoundDependencyException(String message) {
        super(message);
    }
}
