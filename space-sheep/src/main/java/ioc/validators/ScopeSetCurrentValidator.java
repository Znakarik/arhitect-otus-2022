package ioc.validators;

public class ScopeSetCurrentValidator implements Validator {
    private Object[] params;

    public ScopeSetCurrentValidator(Object[] params) {
        this.params = params;
    }

    @Override
    public boolean apply() {
        boolean isLength = params.length == 1;
        if (!isLength) return false;
        boolean allStrings = params[0] instanceof String;
        return allStrings;
    }
}
