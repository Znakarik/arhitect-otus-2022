package ioc.validators;

public class ScopeNewValidator implements Validator {
    private Object[] objects;

    public ScopeNewValidator(Object[] objects) {
        this.objects = objects;
    }

    @Override
    public boolean apply() {
        boolean isLength = objects.length == 1;
        if (!isLength) return false;
        boolean allStrings = objects[0] instanceof String;
        return allStrings;
    }
}
