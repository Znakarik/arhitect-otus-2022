package ioc.validators;

import command.executor.CommandExecutor;
import ioc.constants.IocConstants;

import java.util.function.Function;

public class RegisterScopeAwareValidator implements Validator {

    private Object[] objects;

    public RegisterScopeAwareValidator(Object[] objects) {
        this.objects = objects;
    }

    public boolean apply() {
        boolean lengthValid = (isExecutor() || isExecutorThread())
                ? objects.length == 3 : objects.length == 2;
        boolean firstIsName = objects[0] instanceof String;
        boolean secondIsCreation = objects[1] instanceof Function || objects[1] instanceof Object;
        return lengthValid && firstIsName && secondIsCreation;
    }

    private boolean isExecutorThread() {
        boolean containsExecutorStr = false;
        boolean isExecutorThread = false;
        for (Object o : objects) {
            if (!containsExecutorStr && o instanceof String) {
                containsExecutorStr = o.equals(IocConstants.CommandExecutorThread);
            } else if (o instanceof Thread) {
                isExecutorThread = true;
            }
        }
        return containsExecutorStr && isExecutorThread;
    }

    private boolean isExecutor() {
        boolean containsExecutorStr = false;
        boolean isExecutor = false;
        for (Object o : objects) {
            if (!containsExecutorStr && o instanceof String) {
                containsExecutorStr = o.equals(IocConstants.CommandExecutor);
            } else if (o instanceof CommandExecutor) {
                isExecutor = true;
            }
        }
        return containsExecutorStr && isExecutor;
    }
}
