package ioc.validators;

public interface Validator {
    boolean apply();
}
