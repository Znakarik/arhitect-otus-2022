package ioc.validators;

import adapter.generators.UObject;

public class CreateAdapterImlValidator implements Validator {
    private Object[] params;

    public CreateAdapterImlValidator(Object[] params) {
        this.params = params;
    }

    @Override
    public boolean apply() {
        boolean singleProp = params.length == 1;
        if (!singleProp) return false;
        boolean isClass = params[0] instanceof Class;
        return isClass;
    }

    public boolean applyWithNewUObject() {
        boolean isLengthValid = params.length == 2;
        if (isLengthValid) {
            boolean isClass = params[0] instanceof Class;
            boolean isUObject = params[1] instanceof UObject;
            return isClass && isUObject;
        }
        return false;
    }
}
