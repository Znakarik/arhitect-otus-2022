package ioc;

public class Game {
    private Container container;

    public Game() {
        this.container = new Container();
    }

    public Container getContainer() {
        return container;
    }
}
