package ioc.data;

import exceptions.NotFoundScopeException;
import ioc.Game;

import java.util.HashMap;
import java.util.Map;

public class GameDataRepositoryImpl implements GameDataRepository<Game> {

    private Map<String, Game> storages;
    private String currentId;

    public GameDataRepositoryImpl() {
        this.storages = new HashMap<>();
    }

    private GameDataRepositoryImpl(Map<String, Game> storages, String currentId) {
        this.storages = storages;
        this.currentId = currentId;
    }

    @Override
    public Game setCurrentById(String id) {
        this.currentId = id;
        Game container = getCurrent();
        if (container == null) {
            throw new NotFoundScopeException("Не найден scope с id=" + id);
        }
        return container;
    }

    @Override
    public Game getCurrent() {
        return storages.get(currentId);
    }

    @Override
    public String getCurrentId() {
        return currentId;
    }

    @Override
    public void addNewContainer(String id, Game container) {
        storages.put(id, container);
        if (currentId == null) {
            currentId = id;
        }
    }

    @Override
    public void addNewContainers(Map<String, Game> containers) {
        storages.putAll(containers);
    }

    @Override
    public Game getContainerById(String id) {
        return storages.get(id);
    }

    @Override
    public void clear() {
        storages.clear();
        currentId = null;
    }

    @Override
    public Map<String, Game> getCopyContainer() {
        Map<String, Game> copy = new HashMap<>();
        storages.entrySet().stream().forEach(entry -> copy.put(entry.getKey(), entry.getValue()));
        return copy;
    }
}
