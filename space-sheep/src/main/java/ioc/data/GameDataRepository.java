package ioc.data;

import ioc.Game;

import java.util.List;
import java.util.Map;

public interface GameDataRepository<T> {

    T setCurrentById(String id);

    T getCurrent();

    String getCurrentId();

    void addNewContainer(String id, T container);

    void addNewContainers(Map<String, T> containers);

    T getContainerById(String id);

    void clear();

    Map<String, T> getCopyContainer();
}
