package ioc;

import java.util.HashMap;
import java.util.Map;

public class Container {
    private Map<String, Object> container;

    public Container() {
        this.container = new HashMap<>();
    }

    public Object addNew(String name, Object o) {
        if (container.get(name) == null) {
            container.put(name, o);
        } else {
            container.replace(name, container.get(name), o);
        }
        return o;
    }

    public Object addNew(String name, Object ... o) {
        if (container.get(name) == null) {
            container.put(name, o);
        } else {
            container.replace(name, container.get(name), o);
        }
        return o;
    }

    public Map<String, Object> getContainerCopy() {
        Map<String, Object> map = new HashMap<>(container);
        return map;
    }
}
