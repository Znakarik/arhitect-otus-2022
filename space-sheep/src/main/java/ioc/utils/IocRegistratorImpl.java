package ioc.utils;

import ioc.Game;

public class IocRegistratorImpl implements IocRegistrator {

    private final Game game;

    public IocRegistratorImpl(Game game) {
        this.game = game;
    }

    @Override
    public Object register(String name, Object... params) {
        Object o = game.getContainer().addNew(name, params);
        return o;
    }
}
