package ioc.utils;

public interface IocRegistrator {

    Object register(String name, Object... params);
}
