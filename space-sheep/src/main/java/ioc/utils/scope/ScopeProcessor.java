package ioc.utils.scope;

public interface ScopeProcessor {
    void createNewScope();
}
