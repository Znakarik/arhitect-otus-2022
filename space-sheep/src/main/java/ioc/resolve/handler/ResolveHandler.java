package ioc.resolve.handler;

public interface ResolveHandler {
    Object handle(Object ... params);
}
