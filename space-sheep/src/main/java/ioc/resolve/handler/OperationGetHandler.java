package ioc.resolve.handler;

import ioc.Game;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.validators.Validator;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OperationGetHandler implements ResolveHandler {
    @Override
    public Object handle(Object... params) {
        Object o = null;
        if (new OperationsGetValidator(params).apply()) {
            GameDataRepository<ThreadLocal<Game>> repository = (GameDataRepository<ThreadLocal<Game>>) params[2];
            String iocCode = (String) params[0];
            Object uobj = ((Object[]) params[1])[0];
            Function function = (Function) repository.getCurrent().get().getContainer().getContainerCopy().get(iocCode);
            o = function.apply(uobj);
        }
        return o;
    }

    class OperationsGetValidator implements Validator {
        private Object[] params;

        public OperationsGetValidator(Object[] params) {
            this.params = params;
        }

        @Override
        public boolean apply() {
            boolean lengthValid = params.length == 3;
            if (lengthValid) {
                String name = (String) params[0];
                Object obj = params[1];
                GameDataRepository repository = (GameDataRepository) params[2];
                Pattern pattern = Pattern.compile(IocConstants.methodValueRegex);
                Matcher matcher = pattern.matcher(name);
                return matcher.matches() && obj != null && repository != null;
            }
            return false;
        }
    }
}
