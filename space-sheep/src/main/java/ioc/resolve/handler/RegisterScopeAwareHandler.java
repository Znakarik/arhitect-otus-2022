package ioc.resolve.handler;

import ioc.Game;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.utils.IocRegistratorImpl;
import ioc.validators.RegisterScopeAwareValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RegisterScopeAwareHandler implements ResolveHandler {
    private final GameDataRepository<Game> dataRepository;

    public RegisterScopeAwareHandler(GameDataRepository<Game> dataRepository) {
        this.dataRepository = dataRepository;
    }

    public Object handle(Object... params) {
        Object o;
        if (new RegisterScopeAwareValidator(params).apply()) {
            o = register((String) params[0], removeCommandParam(params));
            if (o instanceof Object[]) {
                o = ((Object[]) o)[0];
            }
        } else throw new IllegalArgumentException("Аргументы неправильные");
        return o;
    }

    private Object register(String name, Object... params) {
        Optional<String> scopeId = getScopeIdIfPresent(params);
        Game game;
        if (scopeId.isPresent()) {
            game = dataRepository.getContainerById(scopeId.get());
        } else {
            game = dataRepository.getCurrent();
        }
        return new IocRegistratorImpl(game).register(name, params);
    }

    private Optional<String> getScopeIdIfPresent(Object... params) {
        for (Object o : params) {
            if (o instanceof String) {
                if (((String) o).contains(IocConstants.scopeIdRegexp)) {
                    String scopeId = o.toString().replaceAll(IocConstants.scopeIdRegexp, "");
                    return Optional.of(scopeId);
                }
            }
        }
        return Optional.empty();
    }

    private Object[] removeCommandParam(Object[] arr) {
        String commandName = (String) arr[0];
        List<Object> listFromParams = new ArrayList<>(Arrays.asList(arr));
        listFromParams.remove(commandName);
        return listFromParams.toArray();
    }

}
