package ioc.resolve.handler;

import adapter.generators.classparts.SimpleClassPartsGenerator;
import exceptions.CouldNotInstantiateClass;
import exceptions.NotFoundInterfaceImplementaionException;
import ioc.constants.IocConstants;
import ioc.Ioc;
import ioc.validators.CreateAdapterImlValidator;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Vector;

public class CreateAdapterHandler implements ResolveHandler {
    @Override
    public Object handle(Object... params) {
        Object o = null;
        CreateAdapterImlValidator validator = new CreateAdapterImlValidator(params);
        if (validator.apply()) {
            Class impl = getImplementedClass((Class) params[0]);
            o = createClassInstance(impl);
            return o;
        } else if (validator.applyWithNewUObject()) {
            Class impl = getImplementedClass((Class) params[0]);
            o = createClassInstance(impl);
            return o;
        }
        return o;
    }

    private Class getImplementedClass(Class type) {
        String generatedClassName = Ioc.Resolve(String.format(IocConstants.generatedClassName, type.getSimpleName()));
        Object[] res = Ioc.Resolve(String.format(IocConstants.generatedClassRegex, generatedClassName));
        if (res != null && res.length == 1) return (Class) res[0];
        Field f = null;
        try {
            f = ClassLoader.class.getDeclaredField("classes");
        } catch (NoSuchFieldException e) {

        }
        f.setAccessible(true);

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Vector<Class> classes = null;
        try {
            classes = (Vector<Class>) f.get(classLoader);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Vector<Class> classesCopy = new Vector<>(classes.size() + 10);
        classesCopy.addAll(classes);
        Iterator<Class> iterator = classesCopy.iterator();
        while (iterator.hasNext()) {
            Class target = iterator.next();
            String targetSimpleName = target.getSimpleName();
            if (targetSimpleName.equals(type.getSimpleName() + "Impl")
                    ||
                    targetSimpleName.equals(Ioc.Resolve(String.format(IocConstants.generatedClassName, type.getSimpleName())))
            ) {
                return target;
            }
        }

        throw new NotFoundInterfaceImplementaionException(
                "Не нашлась имплементация итерфейса " + type.getSimpleName());
    }

    private Object createClassInstance(Class toInstantiate) {
        Object o;
        try {
            Class superInterface = toInstantiate.getInterfaces()[0];
            String uobjectType = String.format(IocConstants.uobjectTypePattern, superInterface.getSimpleName());
            String uobjectValue = String.format(IocConstants.uobjectValuePattern, superInterface.getSimpleName());

            Class classUObj = Ioc.Resolve(uobjectType);
            Object obj = Ioc.Resolve(uobjectValue);
            o = toInstantiate.getDeclaredConstructor(classUObj).newInstance(obj);
        } catch (Exception e) {
            throw new CouldNotInstantiateClass(
                    "Не удалось создать инстанс класса " + toInstantiate.getSimpleName());
        }
        return o;
    }

}
