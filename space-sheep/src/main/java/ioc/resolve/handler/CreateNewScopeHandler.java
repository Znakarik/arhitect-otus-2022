package ioc.resolve.handler;

import ioc.Game;
import ioc.data.GameDataRepository;
import ioc.validators.ScopeNewValidator;

public class CreateNewScopeHandler implements ResolveHandler {
    private final GameDataRepository<Game> dataRepository;

    public CreateNewScopeHandler(GameDataRepository<Game> dataRepository) {
        this.dataRepository = dataRepository;
    }

    @Override
    public Object handle(Object... params) {
        Object o = null;
        ScopeNewValidator validator = new ScopeNewValidator(params);
        if (validator.apply()) {
            o = createNewScope((String) params[0]);
        }
        return o;
    }

    private Object createNewScope(String id) {
        // плохое создание игры, могут быть нужны параметры.
        // Наверно нужна фабрика из следующего ДЗ
        Game game = new Game();
        Game threadLocal = new Game();
        dataRepository.addNewContainer(id, threadLocal);
        return threadLocal;
    }
}
