package ioc.resolve.handler;

import adapter.generators.SimpleUObject;
import ioc.constants.IocConstants;
import ioc.validators.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModifyPropHandler implements ResolveHandler {

    @Override
    public Object handle(Object... params) {
        Object newValue = null;
        if (new ModifyPropValidator(params).apply()) {
            String iocCode = (String) params[0];
            Object[] uobAndNewVal = (Object[]) params[1]; // плохо! может прийти не uobject // todo Uobject сделать интерфейстом
            SimpleUObject uobject = (SimpleUObject) uobAndNewVal[0];
            newValue = uobAndNewVal[1];
            uobject.rewriteProp(getFieldToRewrite(iocCode), newValue);
        }
        return newValue;
    }

    private String getFieldToRewrite(String iocCode) {
        Pattern pattern = Pattern.compile(IocConstants.methodValueRegex);
        Matcher matcher = pattern.matcher(iocCode);
        matcher.find();
        String fieldName = matcher.group("VARIABLE");
        return fieldName;
    }

    class ModifyPropValidator implements Validator {

        private Object[] params;

        public ModifyPropValidator(Object[] params) {
            this.params = params;
        }

        @Override
        public boolean apply() {
            boolean isLengthValid = params.length == 2;
            if (isLengthValid) {
                String name = (String) params[0];
                Object[] uobAndNewVal = (Object[]) params[1];
                Object obj = uobAndNewVal[0];
                Object newValue = uobAndNewVal[1];
                Pattern pattern = Pattern.compile(IocConstants.methodValueRegex);
                Matcher matcher = pattern.matcher(name);
                return matcher.matches() && obj != null && newValue != null;
            }
            return false;
        }
    }
}
