package ioc.resolve.handler;

import ioc.Game;
import ioc.data.GameDataRepository;
import ioc.validators.ScopeSetCurrentValidator;

public class ScopeSetCurrentHandler implements ResolveHandler {
    private final GameDataRepository<Game> dataRepository;

    public ScopeSetCurrentHandler(GameDataRepository<Game> dataRepository) {
        this.dataRepository = dataRepository;
    }

    @Override
    public Object handle(Object... params) {
        Object o = null;
        if (new ScopeSetCurrentValidator(params).apply()) {
            o = dataRepository.setCurrentById((String) params[0]);
        }
        return o;
    }
}
