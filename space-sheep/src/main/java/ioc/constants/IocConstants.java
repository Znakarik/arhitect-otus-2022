package ioc.constants;

import adapter.generators.compile.FilesCompiler;
import org.apache.log4j.Logger;

public class IocConstants {
    private final static Logger LOGGER = Logger.getLogger(FilesCompiler.class);

    public static final String suffixForImplementation = "Impl";
    public static final String uobjectTypePattern = "%s.UObject.type";
    public static final String uobjectValuePattern = "%s.UObject.type.value";
    public static final String uobjectFieldName = "%s.UObject.field.name";
    public static final String generatedClassRegex = "%s.GeneratedClassValue";
    public static final String generatedClassTmpPath = "%s.GeneratedClassPlace";
    /** дефолт имя класса, которое хотим генерить */
    public static final String nameImpl = "%s" + suffixForImplementation;
    /** будущее имя класса, которое хотим генерить     */
    public static final String generatedClassName = "%s.Generation.ClassName";
    public static final String pathForCreation = ""; // not sure
    public static final String iocResolvePattern = "Ioc.Resolve(%s);";
    public static final String iocResolvePatternWithParam = "Ioc.Resolve(%s, %s);";
    public static final String methodValuePattern = "\"%s.Operations.%s:%s.%s\"";
    public static final String methodValueRegex = "(?<CLASS>.+)\\.Operations\\.(?<INTERFACE>.+)\\:(?<VARIABLE>.+)\\.(?<METHOD>.+)";

    public static final String scopeIdRegexp = "scopeId:";
    public static final String CommandExecutor = "Command.Executor";
    public static final String CommandExecutorThread = "Command.Executor.Thread";
    public static final String CommandsQueue = "Commands.Queue";
}
