package ioc.enumerations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public enum Methods {
    REGISTER("IoC.Register"),
    NEW_SCOPE("Scopes.New"),
    SCOPE_CURRENT("Scopes.Current"),
    NOT_SPECIFIED("NOT_SPECIFIED"),
    ADAPTER("Adapter"),
    OPERATIONS_SET(".+\\.Operations\\..+\\:.+\\.set"),
    OPERATIONS_GET(".+\\.Operations\\..+\\:.+\\.get")

    ;

    private String methodName;

    Methods(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public static Methods getByValue(String name) {
        Methods result = NOT_SPECIFIED;

        Optional<Methods> method = Arrays.stream(Methods.values())
                .filter(methods -> methods.methodName.equals(name))
                .findFirst();

        if (method.isPresent()) {
            return method.get();
        }
        else {
            List<Methods> patterns = List.of(OPERATIONS_SET, OPERATIONS_GET);
            Optional<Methods> methodOptional = patterns.stream()
                    .filter(pattern -> Pattern.compile(pattern.methodName).matcher(name).matches())
                    .findFirst();
            if (methodOptional.isPresent()) {
                return methodOptional.get();
            }
        }
        return result;
    }
}
