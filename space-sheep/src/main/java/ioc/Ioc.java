package ioc;

import exceptions.IocNotFoundDependencyException;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import ioc.enumerations.Methods;
import ioc.resolve.handler.*;

import java.util.HashMap;
import java.util.Map;

public class Ioc {
    static GameDataRepository<Game> dataRepository;
    private final static Map<Methods, ResolveHandler> resolveHandlerMap;

    static {
        dataRepository = new GameDataRepositoryImpl();
        resolveHandlerMap = new HashMap<>();
        resolveHandlerMap.put(Methods.REGISTER, getRegisterHandler());
        resolveHandlerMap.put(Methods.NEW_SCOPE, getCreateNewScopeHandler());
        resolveHandlerMap.put(Methods.SCOPE_CURRENT, getScopeSetCurrentHandler());
        resolveHandlerMap.put(Methods.ADAPTER, getCreateAdapterHandler());
        resolveHandlerMap.put(Methods.OPERATIONS_SET, getModifyPropHandler());
        resolveHandlerMap.put(Methods.OPERATIONS_GET, getOperationsGetHandler());
    }

    public static GameDataRepository<Game> loadDataRepository(GameDataRepository<Game> upload) {
        if (dataRepository == null) {
            dataRepository = upload;
        } else {
            dataRepository.addNewContainers(upload.getCopyContainer());
            dataRepository.setCurrentById(upload.getCurrentId());
        }
        return dataRepository;
    }

    public static <T> T Resolve(String name, Object... params) {
        Methods method = Methods.getByValue(name);

        Object o;
        switch (method) {
            case REGISTER:
                o = resolveHandlerMap.get(Methods.REGISTER).handle(params);
                break;
            case NEW_SCOPE:
                o = resolveHandlerMap.get(Methods.NEW_SCOPE).handle(params);
                break;
            case SCOPE_CURRENT:
                o = resolveHandlerMap.get(Methods.SCOPE_CURRENT).handle(params);
                break;
            case ADAPTER:
                o = resolveHandlerMap.get(Methods.ADAPTER).handle(params);
                break;
            case OPERATIONS_SET:
                o = resolveHandlerMap.get(Methods.OPERATIONS_SET).handle(name, params);
                break;
            case OPERATIONS_GET:
                o = resolveHandlerMap.get(Methods.OPERATIONS_GET).handle(name, params, dataRepository);
                break;
            default:
                o = lastTryToResolve(name, params);
        }
        return (T) o;
    }

    private static RegisterScopeAwareHandler getRegisterHandler() {
        return new RegisterScopeAwareHandler(dataRepository);
    }

    private static CreateNewScopeHandler getCreateNewScopeHandler() {
        return new CreateNewScopeHandler(dataRepository);
    }

    private static ScopeSetCurrentHandler getScopeSetCurrentHandler() {
        return new ScopeSetCurrentHandler(dataRepository);
    }

    private static CreateAdapterHandler getCreateAdapterHandler() {
        return new CreateAdapterHandler();
    }

    private static OperationGetHandler getOperationsGetHandler() {
        return new OperationGetHandler();
    }

    private static ModifyPropHandler getModifyPropHandler() {
        return new ModifyPropHandler();
    }

    private static Object lastTryToResolve(String resolvePattern, Object... params) {
        String scopeId = getScopeId(params);
        Map<String, Object> currentScopeContainer;
        if (scopeId != null) {
            Game game = getGame(scopeId);
            currentScopeContainer = game.getContainer().getContainerCopy();
        }
         else {
            currentScopeContainer = dataRepository.getCurrent().getContainer().getContainerCopy();
        }
        if (currentScopeContainer.keySet().contains(resolvePattern)) {
            Object o = currentScopeContainer.get(resolvePattern);
            if (scopeId != null && o instanceof Object[]) {
                return ((Object[]) o)[1];
            } else {
                return o;
            }
        }
        throw new IocNotFoundDependencyException("Не нашли в контейнере обьект по ключу = " + resolvePattern);
    }

    private static String getScopeId(Object... params) {
        String scopeId = null;
        for (Object o : params) {
            if (o instanceof String) {
                boolean isScopeId = ((String) o).contains(IocConstants.scopeIdRegexp);
                if (isScopeId) {
                    scopeId = ((String) o).replaceAll(IocConstants.scopeIdRegexp, "");
                }
            }
        }
        return scopeId;
    }

    private static Game getGame(String scopeId) {
        return dataRepository.getContainerById(scopeId);
    }

}
