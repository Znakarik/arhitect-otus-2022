package adapter;

public class ClassSchema {

    public static final String baseSchema =
            "package %s\n" +
            "%s\n" + // import(s)
            "public class " +
            "%s " + // className
            "%s " + // implements
            "{" +
            "\n%s" + // UObject
            "\n%s" + // constructor
            "\n%s" + // methods
            "\n}";
}
