package adapter.generators.classes;

import adapter.Generator;
import adapter.InterfaceReader;
import adapter.generators.classparts.SimpleClassPartsGenerator;
import adapter.generators.classparts.util.FileClassWriter;
import adapter.generators.classparts.util.PathParserUtil;

import java.io.IOException;
import java.util.Locale;

import static adapter.ClassSchema.baseSchema;

public class SimpleGenerator implements Generator {

    private final Class UObjectClass;

    private final String whereToCreateAdapterPath;

    private final Class interfaceForGeneration;

    private InterfaceReader readerInterface;

    public SimpleGenerator(String whereToCreateAdapterPath, Class interfaceForGeneration) {
        this.whereToCreateAdapterPath = validatePath(whereToCreateAdapterPath);
        this.interfaceForGeneration = validateInterface(interfaceForGeneration);
        UObjectClass = Object.class;
    }

    private Class validateInterface(Class interfaceToUseForGen) {
        boolean isInterface = interfaceToUseForGen.isInterface();
        if (!isInterface)
            throw new IllegalArgumentException("Переданный класс \"" + interfaceToUseForGen.getSimpleName() + "\" не является интерфейсом");
        return interfaceToUseForGen;
    }

    private String validatePath(String path) {
        boolean isJavaClassExpected = path.endsWith(".java");
        if (!isJavaClassExpected)
            throw new IllegalArgumentException("Переданный путь \"" + path + "\" не является интерфейсом");
        return path;
    }

    @Override
    public void generate() throws IOException {
        String pathWithoutClassName = PathParserUtil.clearPathFromFileName(whereToCreateAdapterPath);
        String methods = SimpleClassPartsGenerator.generateMethods(interfaceForGeneration);
        String fullClass = generateClass(pathWithoutClassName, methods);
        FileClassWriter.writeClass(whereToCreateAdapterPath, fullClass);
    }

    private String generateUObjectField() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("private %s %s;",
                UObjectClass.getSimpleName(), UObjectClass.getSimpleName().toLowerCase(Locale.ROOT)));
        return builder.toString();
    }

    private String generateClass(String packagePath, String methods) {
        String stringBuilder =
                String.format(
                        baseSchema,
                        SimpleClassPartsGenerator.generatePackage(packagePath),
                        SimpleClassPartsGenerator.generateImport(interfaceForGeneration),
                        SimpleClassPartsGenerator.generateClassName(interfaceForGeneration),
                        SimpleClassPartsGenerator.generateImplements(interfaceForGeneration),
                        generateUObjectField(),
                        methods);
        return stringBuilder;
    }
}
