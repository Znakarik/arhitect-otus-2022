package adapter.generators.classes;

import adapter.ClassSchema;
import adapter.Generator;
import adapter.generators.classparts.SimpleClassPartsGenerator;
import adapter.generators.classparts.util.FileClassWriter;
import adapter.generators.classparts.util.PathParserUtil;
import adapter.generators.compile.FilesCompiler;
import ioc.constants.IocConstants;
import ioc.Ioc;
import org.apache.log4j.Logger;

import java.io.File;

public class GenerateWithStrategyFromIoc implements Generator {
    private final static Logger LOGGER = Logger.getLogger(GenerateWithStrategyFromIoc.class);

    private final String whereToCreateAdapterPath;

    private final Class interfaceForGeneration;

    private final FilesCompiler compiler;

    public GenerateWithStrategyFromIoc(
                            String whereToCreateAdapterPath,
                            Class interfaceForGeneration, FilesCompiler compiler) {
        this.whereToCreateAdapterPath = whereToCreateAdapterPath;
        this.interfaceForGeneration = interfaceForGeneration;
        this.compiler = compiler;
    }

    @Override
    public void generate() {
        Class classUObj = Ioc.Resolve(
                String.format(IocConstants.uobjectTypePattern,
                        interfaceForGeneration.getSimpleName()));
        String packageValue = SimpleClassPartsGenerator.
                generatePackage(PathParserUtil.clearPathFromFileName(whereToCreateAdapterPath));

        String fullClass = String.format(ClassSchema.baseSchema,
                packageValue,
                generateImports(classUObj),
                generateClassName(),
                SimpleClassPartsGenerator.generateImplements(interfaceForGeneration),
                SimpleClassPartsGenerator.generateField(interfaceForGeneration, classUObj),
                SimpleClassPartsGenerator.generateConstructor(
                        interfaceForGeneration, classUObj
                ),
                SimpleClassPartsGenerator.generateMethodsWithIoc(interfaceForGeneration)
        );

        LOGGER.info(" ========== Сгенерировали класс ========== \n" + fullClass);
        File file = FileClassWriter.writeClass(whereToCreateAdapterPath, fullClass);
        compiler.compile(file);
    }

    private String generateImports(Class classUObj) {
        return SimpleClassPartsGenerator.generateImport(interfaceForGeneration) +
                SimpleClassPartsGenerator.generateImport(classUObj) +
                SimpleClassPartsGenerator.generateImport(Ioc.class) +
                "\n" +
                SimpleClassPartsGenerator.generateImportsFromInterface(interfaceForGeneration);
    }

    private String generateClassName() {
        return Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGeneration.getSimpleName()));
    }
}
