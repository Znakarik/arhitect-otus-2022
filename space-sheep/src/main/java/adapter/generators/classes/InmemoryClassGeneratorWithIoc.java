package adapter.generators.classes;

import adapter.ClassSchema;
import adapter.Generator;
import adapter.generators.classparts.SimpleClassPartsGenerator;
import adapter.generators.classparts.util.PathParserUtil;
import adapter.generators.compile.inmemory.InMemoryCompiler;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.enumerations.Methods;
import org.apache.log4j.Logger;

/**
 * Компиляция классов в tmp дерикториях
 */
public class InmemoryClassGeneratorWithIoc implements Generator {
    private final static Logger LOGGER = Logger.getLogger(GenerateWithStrategyFromIoc.class);

    private final String whereToCreateAdapterPath;

    private final Class interfaceForGeneration;

    public InmemoryClassGeneratorWithIoc(
            String whereToCreateAdapterPath,
            Class interfaceForGeneration) {
        this.whereToCreateAdapterPath = whereToCreateAdapterPath;
        this.interfaceForGeneration = interfaceForGeneration;
    }

    @Override
    public void generate() {
        Class classUObj = Ioc.Resolve(
                String.format(IocConstants.uobjectTypePattern,
                        interfaceForGeneration.getSimpleName()));
        String packageValue = SimpleClassPartsGenerator.
                generatePackage(PathParserUtil.clearPathFromFileName(whereToCreateAdapterPath));

        String fullClass = String.format(ClassSchema.baseSchema,
                packageValue,
                generateImports(classUObj),
                generateClassName(),
                SimpleClassPartsGenerator.generateImplements(interfaceForGeneration),
                SimpleClassPartsGenerator.generateField(interfaceForGeneration, classUObj),
                SimpleClassPartsGenerator.generateConstructor(
                        interfaceForGeneration, classUObj
                ),
                SimpleClassPartsGenerator.generateMethodsWithIoc(interfaceForGeneration)
        );

        LOGGER.info(" ========== Сгенерировали класс ========== \n" + fullClass);
        InMemoryCompiler compiler = new InMemoryCompiler(PathParserUtil.getPackageNameFromClass(fullClass), fullClass);
        Class target = compiler.compile();
        Ioc.Resolve(Methods.REGISTER.getMethodName(), String.format(IocConstants.generatedClassRegex, generateClassName()), target);
    }

    private String generateImports(Class classUObj) {
        return
                SimpleClassPartsGenerator.generateImport(classUObj) +
                        SimpleClassPartsGenerator.generateImport(Ioc.class) +
                        SimpleClassPartsGenerator.generateImport(interfaceForGeneration) +
                        "\n" +
                SimpleClassPartsGenerator.generateImportsFromInterface(interfaceForGeneration);
    }

    private String generateClassName() {
        return Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGeneration.getSimpleName()));
    }
}
