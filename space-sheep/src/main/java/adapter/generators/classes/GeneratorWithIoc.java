package adapter.generators.classes;

import adapter.ClassSchema;
import adapter.Generator;
import adapter.generators.classparts.SimpleClassPartsGenerator;
import adapter.generators.classparts.util.FileClassWriter;
import adapter.generators.classparts.util.PathParserUtil;
import adapter.generators.compile.FilesCompiler;

import ioc.constants.IocConstants;
import ioc.Ioc;

import java.io.File;

/**
 * Генерирует класс на основе информации находящейся в Ioc
 */
public class GeneratorWithIoc implements Generator {
    private final String whereToCreateAdapterPath;

    private final Class interfaceForGeneration;

    private final FilesCompiler compiler;

    public GeneratorWithIoc(String whereToCreateAdapterPath,
                            Class interfaceForGeneration, FilesCompiler compiler) {
        this.whereToCreateAdapterPath = whereToCreateAdapterPath;
        this.interfaceForGeneration = interfaceForGeneration;
        this.compiler = compiler;
    }

    @Override
    public void generate() {
        Class classUObj = Ioc.Resolve(
                String.format(IocConstants.uobjectTypePattern,
                interfaceForGeneration.getSimpleName()));
        String packageValue = SimpleClassPartsGenerator.
                generatePackage(PathParserUtil.clearPathFromFileName(whereToCreateAdapterPath));

        String fullClass = String.format(ClassSchema.baseSchema,
                packageValue,
                SimpleClassPartsGenerator.generateImport(interfaceForGeneration),
                generateClassName(),
                SimpleClassPartsGenerator.generateImplements(interfaceForGeneration),
                SimpleClassPartsGenerator.generateField(interfaceForGeneration, classUObj),
                SimpleClassPartsGenerator.generateConstructor(
                        interfaceForGeneration, classUObj
                ),
                SimpleClassPartsGenerator.generateMethods(interfaceForGeneration)
                );

        File file = FileClassWriter.writeClass(whereToCreateAdapterPath, fullClass);
        compiler.compile(file);
    }

    private String generateClassName() {
        return Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGeneration.getSimpleName()));
    }
}
