package adapter.generators;

import ioc.Game;
import ioc.constants.IocConstants;

/**
 * Класс, который знает где создавать
 */
public class IocObjectAware implements ObjectAware {
    private final Class from;
    private final String classTo;
    private final Object uobject;
    private final Game game;

    public IocObjectAware(Game game, Class from, Object uobject) {
        this.from = from;
        this.classTo = from.getSimpleName() + IocConstants.suffixForImplementation;
        this.uobject = uobject;
        this.game = game;
    }

    public void apply() {
        game.getContainer().addNew(String.format(IocConstants.uobjectTypePattern, from.getSimpleName()), uobject.getClass());
        game.getContainer().addNew(String.format(IocConstants.uobjectValuePattern, from.getSimpleName()), uobject);
        game.getContainer().addNew(String.format(IocConstants.nameImpl, from), from);
    }
}