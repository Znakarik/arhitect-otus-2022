package adapter.generators.classparts;

import adapter.generators.MethodGenerator;
import ioc.Ioc;
import ioc.constants.IocConstants;

public class ConstructorGeneratorImpl implements MethodGenerator {
    private final Class interfaceForGeneration;
    private final Class uobject;

    public ConstructorGeneratorImpl(Class interfaceForGeneration, Class uobject) {
        this.interfaceForGeneration = interfaceForGeneration;
        this.uobject = uobject;
    }

    @Override
    public String generate() {
        StringBuilder stringBuilder = new StringBuilder();
        String uobjectName = Ioc.Resolve(String.format(IocConstants.uobjectFieldName, interfaceForGeneration.getSimpleName()));
        String className = Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGeneration.getSimpleName()));
        stringBuilder.append("public")
                .append(" ")
                .append(className)
                .append("(")
                .append(uobject.getSimpleName())
                .append(" ")
                .append(uobjectName)
                .append(") {\n")
                .append("this.")
                .append(uobjectName)
                .append(" = ")
                .append(uobjectName)
                .append(";\n")
                .append("}\n");

        return stringBuilder.toString();
    }
}
