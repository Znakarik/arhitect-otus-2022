package adapter.generators.classparts;

import exceptions.NotFoundMethodStrategyException;


import java.lang.reflect.Method;

public class MethodGenerator {

    public static String generateMethod(Method method, Class interfaceForGen) {
        Methods methodType = Methods.find(method);
        switch (methodType) {
            case GETTER:
                return new GetterGenerator(method, interfaceForGen).generate();
            case SETTER:
                return new SetterGenerator(method, interfaceForGen).generate();
        }
        throw new NotFoundMethodStrategyException();
    }

    public static String generateMethodWithIoc(Method method, Class interfaceForGen) {
        Methods methodType = Methods.find(method);
        switch (methodType) {
            case GETTER:
                return new GetterGeneratorWithIoc(method, interfaceForGen).generate();
            case SETTER:
                return new SetterGeneratorWithIoc(method, interfaceForGen).generate();
        }
        throw new NotFoundMethodStrategyException();
    }
}
