package adapter.generators.classparts.util;

import exceptions.WriteClassException;

import java.io.File;
import java.io.FileWriter;

public class FileClassWriter implements ClassWriter {

    public static File writeClass(String path, String classAsString) {
        File file = new File(path);
        try (FileWriter writer = new FileWriter(file)){
            writer.write(classAsString);
        } catch (Exception e) {
            throw new WriteClassException(e);
        }
        return file;
    }
}
