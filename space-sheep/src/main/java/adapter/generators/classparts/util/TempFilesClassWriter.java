package adapter.generators.classparts.util;

import adapter.generators.classparts.SimpleClassPartsGenerator;
import exceptions.CreateClassException;
import exceptions.WriteClassException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TempFilesClassWriter implements ClassWriter {

    public static File writeClass(String classAsString) {
        String className = PathParserUtil.getClassNameFromStrClass(classAsString);
        File file = createTemp(className);
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(classAsString);
        } catch (Exception e) {
            throw new WriteClassException(e);
        }
        return file;
    }

    private static File createTemp(String className) {
        File res = null;
        try {
            String pwd = new File(".").getCanonicalPath();
            res = new File(pwd + "/src/main/java/generated/adapters/" + className + ".java");
            res.deleteOnExit();
        } catch (IOException e) {
            throw new CreateClassException("Не удалось создать файл класса " + className, e);
        }
        return res;
    }
}
