package adapter.generators.classparts.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathParserUtil {

    /**
     * Генерирует имя класса из пути
     * /src/main/java/dto/SomeClass.java"
     * @return "SomeClass"
     */
    public static String getClassNameToGenerate(String path) {
        Pattern pattern = Pattern.compile("[\\/\\w+\\/]{0,}(?<END>\\/\\w+\\.java$)");
        Matcher matcher = pattern.matcher(path);
        matcher.find();
        String endOfPath = matcher.group("END");
        return endOfPath
                .replaceAll("\\/", "")
                .replaceAll(".java", "");
    }

    /**
     * Чистит путь от класса
     * @param path "/src/main/java/dto/SomeClass.java"
     * @return "/src/main/java/dto/"
     */
    public static String clearPathFromFileName(String path) {
        Pattern pattern = Pattern.compile("[\\/\\w+\\/]{0,}(?<END>\\/\\w+\\.java$)");
        Matcher matcher = pattern.matcher(path);
        matcher.find();
        String endOfPath = matcher.group("END");
        return path.replaceAll(endOfPath, "");
    }

    public static String clearPathToPackage(String path) {
        Pattern pattern = Pattern.compile(".*/java/(?<PACKAGE>.+)");
        Matcher matcher = pattern.matcher(path);
        matcher.find();
        String target = matcher.group("PACKAGE");
        if (target != null && !target.isEmpty()) return target;
        throw new IllegalArgumentException();
    }

    /**
     * Приводит строку ru.znakarik.Tank к Tank
     */
    public static String clearClassNameFromPackage(String strPackage) {
        if (strPackage.matches("(.+(\\.)?){1,}")) {
            return strPackage.substring(strPackage.lastIndexOf(".") + 1);
        }
        throw new IllegalArgumentException(String.format("Строка \"%s\" не содержит пакет", strPackage));
    }

    /**
     * Находит в содержимом класса имя класса
     * @param strClass
     * @return
     */
    public static String getClassNameFromStrClass(String strClass) {
        if (strClass.contains("class")) {
            String[] split = strClass.split(" ");
            String className = null;
            for (int i = 0; i < split.length - 1; i++) {
                if (split[i].equals("class")) {
                    className = split[i + 1];
                }
            }
            if (className == null) throw new IllegalArgumentException();
            return className;
        }
        throw new IllegalArgumentException();
    }

    public static String replaceJavaToClass(String path) {
        if (path.matches(".+\\.java")) {
            return path.replace(".java", ".class");
        }
        throw new IllegalArgumentException();
    }

    public static String getPackageNameFromClass(String fullClass) {
        String packagePart = fullClass.substring(0, fullClass.indexOf(";"))
                .replace("package ", "");

        String className = getClassNameFromStrClass(fullClass);

        return packagePart + "." + className;
    }
}
