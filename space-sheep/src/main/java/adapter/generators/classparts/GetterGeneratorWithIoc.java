package adapter.generators.classparts;

import exceptions.IocNotFoundDependencyException;

import ioc.constants.IocConstants;
import ioc.Ioc;

import java.lang.reflect.Method;

public class GetterGeneratorWithIoc extends GetterGenerator {

    public GetterGeneratorWithIoc(Method method, Class interfaceForGen) {
        super(method, interfaceForGen);
    }

    @Override
    protected String generateValue() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("return").append(" ");

        String targetClass;
        try {
            targetClass = Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGen.getSimpleName()));
        } catch (IocNotFoundDependencyException e) {
            targetClass = Ioc.Resolve(String.format(IocConstants.nameImpl, interfaceForGen.getSimpleName()));
        }
        String getValueStrategyFromIocCode = String.format(IocConstants.methodValuePattern, targetClass, interfaceForGen.getSimpleName(), new GetFieldNameFromMethod(method).apply(), "get");
        String uobjectFieldName = Ioc.Resolve(String.format(IocConstants.uobjectFieldName, interfaceForGen.getSimpleName()));
        stringBuilder.append(String.format(
                IocConstants.iocResolvePatternWithParam, getValueStrategyFromIocCode, uobjectFieldName
        ));
        return stringBuilder.toString();
    }
}
