package adapter.generators.classparts;

import adapter.generators.MethodGenerator;
import adapter.generators.classparts.GetFieldNameFromMethod;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class SetterGenerator implements MethodGenerator {
    protected final Method method;
    protected final Class interfaceForGen;

    public SetterGenerator(Method method, Class interfaceForGen) {
        this.method = method;
        this.interfaceForGen = interfaceForGen;
    }

    @Override
    public String generate() {
        StringBuilder stringBuilder = new StringBuilder();

        Class returnValue = method.getReturnType();
        String methodName = method.getName();
        Parameter[] params = method.getParameters();
        stringBuilder.append("public");
        stringBuilder.append(" ");
        stringBuilder.append(returnValue.getSimpleName());
        stringBuilder.append(" ");
        stringBuilder.append(methodName);
        stringBuilder.append("(");
        for (int i = 0; i <= params.length - 1; i++) {
            stringBuilder.append(params[i].getType().getSimpleName());
            stringBuilder.append(" ");
            stringBuilder.append(new GetFieldNameFromMethod(method).apply());
            if (i != params.length - 1) {
                stringBuilder.append(", ");
            }
        }
        stringBuilder.append(")");
        stringBuilder.append("{\n");
        stringBuilder.append(generateValue(params));
        stringBuilder.append("\n");
        stringBuilder.append("}\n");
        return stringBuilder.toString();
    }

    protected String getFieldNameFromMethod() {
        return new GetFieldNameFromMethod(method).apply();
    }

    protected String generateValue(Parameter[] params) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Parameter parameter : params) {
            String fieldName = getFieldNameFromMethod();
            stringBuilder.append(String.format("this.%s = %s;", fieldName,fieldName));
        }
        return stringBuilder.toString();
    }
}
