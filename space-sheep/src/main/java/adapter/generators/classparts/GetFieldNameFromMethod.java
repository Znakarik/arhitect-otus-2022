package adapter.generators.classparts;

import java.lang.reflect.Method;
import java.util.Locale;

public class GetFieldNameFromMethod {
    private Method method;

    public GetFieldNameFromMethod(Method method) {
        this.method = method;
    }

    public String apply() {
        String result = method.getName().replaceAll("get|set", "").toLowerCase(Locale.ROOT);
        return result;
    }
}

