package adapter.generators.classparts;

import adapter.generators.MethodGenerator;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class GetterGenerator implements MethodGenerator {
    protected final Method method;
    protected final Class interfaceForGen;

    public GetterGenerator(Method method, Class interfaceForGen) {
        this.method = method;
        this.interfaceForGen = interfaceForGen;
    }

    @Override
    public String generate() {
        StringBuilder stringBuilder = new StringBuilder();
        Class returnValue = method.getReturnType();
        String methodName = method.getName();
        Parameter[] params = method.getParameters();
        stringBuilder.append("public");
        stringBuilder.append(" ");
        stringBuilder.append(returnValue.getSimpleName());
        stringBuilder.append(" ");
        stringBuilder.append(methodName);
        stringBuilder.append("(");
        if (params.length != 0) {
            for (Parameter param : params) {
            }
        }

        stringBuilder.append(")");
        stringBuilder.append("{\n");
        stringBuilder.append(generateValue());
        stringBuilder.append("\n}");
        return stringBuilder.toString();
    }

    protected String generateValue() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("  return this.").append(new GetFieldNameFromMethod(method).apply()).append(";");
        return stringBuilder.toString();
    }
}
