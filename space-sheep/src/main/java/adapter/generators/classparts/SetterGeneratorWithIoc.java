package adapter.generators.classparts;

import exceptions.IocNotFoundDependencyException;
import ioc.constants.IocConstants;
import ioc.Ioc;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class SetterGeneratorWithIoc extends SetterGenerator {
    public SetterGeneratorWithIoc(Method method, Class interfaceForGen) {
        super(method, interfaceForGen);
    }

    @Override
    protected String generateValue(Parameter[] params) {
        StringBuilder stringBuilder = new StringBuilder();
        String targetClass;
        try {
            targetClass = Ioc.Resolve(String.format(IocConstants.generatedClassName, interfaceForGen.getSimpleName()));
        } catch (IocNotFoundDependencyException e) {
            targetClass = Ioc.Resolve(String.format(IocConstants.nameImpl, interfaceForGen.getSimpleName()));
        }

        if (!method.getReturnType().getSimpleName().equals("void")) {
            stringBuilder.append("return ");
        }
        for (Parameter parameter : params) {
            String setValueStrategy = String.format(
                    IocConstants.methodValuePattern,
                    targetClass,
                    interfaceForGen.getSimpleName(),
                    new GetFieldNameFromMethod(method).apply(),
                    "set");
            String uobjectFieldName = Ioc.Resolve(String.format(IocConstants.uobjectFieldName, interfaceForGen.getSimpleName()));
            String newValueFieldName = getFieldNameFromMethod();
            String paramsForResolve = setValueStrategy + ", " + uobjectFieldName + ", " + newValueFieldName;
            String fullStrategy = String.format(IocConstants.iocResolvePattern, paramsForResolve);
            stringBuilder.append(fullStrategy);
        }
        return stringBuilder.toString();
    }
}
