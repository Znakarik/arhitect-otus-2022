package adapter.generators.classparts;

import ioc.Ioc;
import ioc.constants.IocConstants;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class SimpleClassPartsGenerator {

    /**
     * Генерирует package класса
     *
     * @param packagePath - передавать директорию, где будет создаваться класс
     * "/src/main/java/generated/adapters/"
     * @return package generated.adapters;
     */
    public static String generatePackage(String packagePath) {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> packageArray = Arrays.asList(packagePath.split("/"));
        int indexOfJava = packageArray.lastIndexOf("java");
        for (int i = indexOfJava + 1; i <= packageArray.size() - 1; i++) {
            stringBuilder.append(packageArray.get(i));
            if (i != packageArray.size() - 1) {
                stringBuilder.append(".");
            }
        }
        stringBuilder.append(";");
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    /**
     * Генерирует строку импорта
     * @return import java.util.Arrays;
     */
    public static String generateImport(Class classToImport) {
        return "import " + classToImport.getPackageName() + "." + classToImport.getSimpleName() + ";";
    }

    /**
     * Генерирует строки импорта анализируя обьекты методов
     * @return import java.util.Arrays;
     */
    public static String generateImportsFromInterface(Class interfaceForGeneration) {
        StringBuilder stringBuilder = new StringBuilder();
        Method[] methods = interfaceForGeneration.getDeclaredMethods();
        Arrays.stream(methods).forEach(method -> stringBuilder.append(generateImport(method.getReturnType())).append("\n"));
        Arrays.stream(methods).forEach(method -> Arrays.stream(method.getParameterTypes())
                .forEach(param -> stringBuilder.append(generateImport(param)).append("\n")));
        String result = stringBuilder.toString();
        if (result.contains("import java.lang.void;")) {
            result = result.replaceAll("import java.lang.void;", "");
        }
        return result;
    }

    /**
     * Генерирует " implements SomeClass"
     */
    public static String generateImplements(Class owner) {
        return " implements " + owner.getSimpleName();
    }

    /**
     * Генерирует "SomeInterfaceIml"
     */
    public static String generateClassName(Class ownerName) {
        return ownerName.getSimpleName() + "Impl";
    }

    /**
     * Генерирует private поле по типу
     */
    public static String generateField(Class interfaceForGeneration, Class typeField) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("private %s %s;",
                typeField.getSimpleName(),
                Ioc.Resolve(String.format(IocConstants.uobjectFieldName, interfaceForGeneration.getSimpleName()))));
        return builder.toString();
    }

    public static String generateMethods(Class interfaceToGen) {
        Method[] methods = interfaceToGen.getDeclaredMethods();
        StringBuilder methodsStringBuilder = new StringBuilder();
        for (Method method : methods) {
            methodsStringBuilder.append("\n");
            methodsStringBuilder.append(MethodGenerator.generateMethod(method, interfaceToGen));
        }
        return methodsStringBuilder.toString();
    }

    public static String generateMethodsWithIoc(Class interfaceToGen) {
        Method[] methods = interfaceToGen.getDeclaredMethods();
        StringBuilder methodsStringBuilder = new StringBuilder();
        for (Method method : methods) {
            methodsStringBuilder.append("\n");
            methodsStringBuilder.append(MethodGenerator.generateMethodWithIoc(method, interfaceToGen));
        }
        return methodsStringBuilder.toString();
    }

    public static String generateConstructor(Class interfaceForGeneration, Class uobject) {
        return new ConstructorGeneratorImpl(interfaceForGeneration, uobject).generate();
    }

}
