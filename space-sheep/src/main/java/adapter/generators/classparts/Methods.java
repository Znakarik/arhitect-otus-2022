package adapter.generators.classparts;

import exceptions.NotFoundMethodStrategyException;

import java.lang.reflect.Method;

public enum Methods {
    GETTER, SETTER, CONSTRUCTOR;

    public static Methods find(Method method) {
        if (method.getName().matches("^get.+")) {
            return GETTER;
        } else if (method.getName().matches("^set.+")) {
            return SETTER;
        }
        throw new NotFoundMethodStrategyException();
    }
}

