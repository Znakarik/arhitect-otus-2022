package adapter.generators;

public interface UObject {
    void addNewProp(String name, Object value);
    void rewriteProp(String name, Object newValue);
    Object getProp(String name);
}
