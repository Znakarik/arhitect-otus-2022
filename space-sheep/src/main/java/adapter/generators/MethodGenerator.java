package adapter.generators;

public interface MethodGenerator {
    String generate();
}
