package adapter.generators.compile;

public interface StringCompiler {

    public Class compile();
}
