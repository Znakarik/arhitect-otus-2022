package adapter.generators.compile;

import adapter.generators.classparts.SimpleClassPartsGenerator;
import adapter.generators.classparts.util.PathParserUtil;
import exceptions.CreateClassException;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.enumerations.Methods;
import org.apache.log4j.Logger;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;

public class FilesCompiler {
    private final static Logger LOGGER = Logger.getLogger(FilesCompiler.class);

    public Class compile(File file) {
        Class result = null;
        LOGGER.info("Компилируем класс:" + file.getAbsolutePath());
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, file.getAbsolutePath());

        String name = PathParserUtil.clearPathToPackage(file.getAbsolutePath())
                .replaceAll(".java", "")
                .replaceAll("/", ".");
        try {
            String compiledClass = PathParserUtil.replaceJavaToClass(file.getAbsolutePath());
            File compiledClassFile = Paths.get(compiledClass).toFile();
            URL url = compiledClassFile.toURL();

            ClassLoader classLoader = new CustomClassLoader(
                    url,
                    CustomClassLoader.class.getClassLoader(), name);

            result = classLoader.loadClass(name);
        } catch (Exception e) {
            throw new CreateClassException(e);
        }
        Ioc.Resolve(
                Methods.REGISTER.getMethodName(),
                String.format(IocConstants.generatedClassRegex, PathParserUtil.clearClassNameFromPackage(name)), result);

        return result;
    }

    class CustomClassLoader extends ClassLoader {
        private final URL url;
        private final String classForGeneration;

        public CustomClassLoader(URL url, ClassLoader parent, String classForGeneration) {
            super(parent);
            this.url = url;
            this.classForGeneration = classForGeneration;
        }

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            if (!name.equals(classForGeneration)) {
                return super.loadClass(name);
            }

            byte[] classData;
            try {
                URLConnection connection = url.openConnection();
                InputStream input = connection.getInputStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int data = input.read();

                while (data != -1) {
                    buffer.write(data);
                    data = input.read();
                }

                input.close();

                classData = buffer.toByteArray();

            } catch (IOException e) {
                throw new CreateClassException("Не удалось создать класс " + name, e);
            }

            return defineClass(name, classData, 0, classData.length);
        }
    }

    //    public void compile(String pathJavaFile) {
//        LOGGER.info("Компилируем класс:" + pathJavaFile);
//        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//        compiler.run(null, null, null, pathJavaFile);
//        try {
//
//            ClassLoader.getSystemClassLoader().loadClass(PathParserUtil.clearPathToPackage(pathJavaFile));
//        } catch (ClassNotFoundException e) {
//            throw new CreateClassException(e);
//        }
//    }
}
