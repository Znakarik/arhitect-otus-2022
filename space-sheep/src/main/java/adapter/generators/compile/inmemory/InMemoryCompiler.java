package adapter.generators.compile.inmemory;

import adapter.generators.compile.StringCompiler;
import adapter.generators.compile.inmemory.util.Compile;
import exceptions.CreateClassException;

public class InMemoryCompiler implements StringCompiler {
    private final String name;
    private final String value;

    public InMemoryCompiler(String name, String value) {
        this.name = name;
        this.value = value;
    }

    private Class compile(String name, String value) {
        Class result;
        try {
            result = Compile.compile(name, value);
        } catch (Exception e) {
            throw new CreateClassException(
                    "Не удалось скомпилировать класс \"" + name + "\" в рантайме", e);
        }
        return result;
    }

    @Override
    public Class compile() {
        return compile(name, value);
    }
}
