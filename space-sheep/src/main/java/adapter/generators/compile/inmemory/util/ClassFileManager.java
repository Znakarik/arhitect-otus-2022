package adapter.generators.compile.inmemory.util;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.StandardJavaFileManager;

public final class ClassFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
    public JavaFileObject o;

    ClassFileManager(StandardJavaFileManager m) {
        super(m);
    }

    @Override
    public javax.tools.JavaFileObject getJavaFileForOutput(
            JavaFileManager.Location location,
            String className,
            javax.tools.JavaFileObject.Kind kind,
            FileObject sibling
    ) {
        return o = new JavaFileObject(className, kind);
    }
}