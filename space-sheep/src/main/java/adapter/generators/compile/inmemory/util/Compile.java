package adapter.generators.compile.inmemory.util;


import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;

public class Compile {

    public static Class<?> compile(String className, String content)
            throws Exception {
        MethodHandles.Lookup lookup = MethodHandles.lookup();

        // If we have already compiled our class, simply load it
        try {
            return lookup.lookupClass()
                    .getClassLoader()
                    .loadClass(className);
        }

        // Otherwise, let's try to compile it
        catch (ClassNotFoundException ignore) {
            return compile0(className, content, lookup);
        }
    }

    static Class<?> compile0(
            String className, String content, MethodHandles.Lookup lookup)
            throws Exception {
        JavaCompiler compiler =
                ToolProvider.getSystemJavaCompiler();

        ClassFileManager manager = new ClassFileManager(
                compiler.getStandardFileManager(null, null, null));

        List<CharSequenceJavaFileObject> files = new ArrayList<>();
        files.add(new CharSequenceJavaFileObject(
                className, content));

        compiler.getTask(null, manager, null, null, null, files)
                .call();
        Class<?> result = null;

        // Lookup.defineClass() has only been introduced in Java 9.
        // It is required to get private-access to interfaces in
        // the class hierarchy

        // This method is called by client code from two levels
        // up the current stack frame. We need a private-access
        // lookup from the class in that stack frame in order
        // to get private-access to any local interfaces at
        // that location.
        Class<?> caller = StackWalker
                .getInstance(RETAIN_CLASS_REFERENCE)
                .walk(s -> s
                        .skip(2)
                        .findFirst()
                        .get()
                        .getDeclaringClass());

        // If the compiled class is in the same package as the
        // caller class, then we can use the private-access
        // Lookup of the caller class
        if (className.startsWith(caller.getPackageName())) {
            result = MethodHandles
                    .privateLookupIn(caller, lookup)
                    .defineClass(manager.o.getBytes());
        }

        // Otherwise, use an arbitrary class loader. This
        // approach doesn't allow for loading private-access
        // interfaces in the compiled class's type hierarchy
        else {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

            result = new Loader(manager, classLoader).loadClass(className);
        }

        return result;
    }

    static class Loader extends URLClassLoader {
        private final ClassFileManager manager;
        private final ClassLoader parent;

        public Loader(ClassFileManager manager, ClassLoader parent) {
            super(new URL[]{}, parent);
            this.manager = manager;
            this.parent = parent;
        }

        @Override
        protected Class<?> findClass(String name)
                throws ClassNotFoundException {
            byte[] b = manager.o.getBytes();
            int len = b.length;
            return defineClass(name, b, 0, len);
        }
    }

}
