package adapter.generators;

import java.util.HashMap;
import java.util.Map;

public class SimpleUObject implements UObject {
    private Map<String, Object> props;

    public SimpleUObject() {
        props = new HashMap<>();
    }

    public void addNewProp(String name, Object value) {
        props.put(name, value);
    }

    public void rewriteProp(String name, Object newValue) {
        props.replace(name, props.get(name), newValue);
    }

    public Object getProp(String name) {
        return props.get(name);
    }
}
