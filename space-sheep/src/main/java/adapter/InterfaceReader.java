package adapter;

public interface InterfaceReader {
    String readAndGet(Class clazz);
}
