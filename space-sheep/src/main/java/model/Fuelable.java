package model;

public interface Fuelable {
    int getFuel();

    void setFuel(int newFuelAmount);

    int getMinimumFuel();

    int getBurnStepAmount();
}
