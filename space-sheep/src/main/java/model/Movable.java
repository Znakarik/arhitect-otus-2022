package model;

import command.move.Position;
import command.move.Velocity;

public interface Movable {
    Position getPosition();

    Velocity getVelocity();

    void setPosition(Position newValue);
}
