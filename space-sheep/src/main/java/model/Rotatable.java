package model;

public interface Rotatable {
    // секция круга
    int getDirection();

    // d * a
    int getAngularVelocity();

    void setDirection(int newValue);

    // кол-во секций круга
    int getDirectionsNumber();
}
