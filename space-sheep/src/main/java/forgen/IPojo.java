package forgen;

public interface IPojo {
    Object getObject();

    void setObject(Object object);
}
