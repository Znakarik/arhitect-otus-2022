package command.fuel.check;

import command.Command;
import exceptions.CheckFuelCommandException;
import model.Fuelable;

public class CheckFuelCommand implements Command {

    private Fuelable fuelable;

    public CheckFuelCommand(Fuelable fuelable) {
        this.fuelable = fuelable;
    }

    @Override
    public void execute() {
        try {
            validateFuel();
            checkFuel();
        } catch (Exception e) {
            throw new CheckFuelCommandException(e);
        }
    }

    private void checkFuel() {
        if (fuelable.getFuel() < fuelable.getMinimumFuel()) {
            throw new CheckFuelCommandException("Не достаточно топлива");
        }
    }

    private void validateFuel() {
        if (fuelable == null || fuelable.getFuel() < 0 || fuelable.getMinimumFuel() < 0) {
            throw new IllegalArgumentException();
        }
    }
}
