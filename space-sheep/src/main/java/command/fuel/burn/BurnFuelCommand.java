package command.fuel.burn;

import command.Command;
import exceptions.BurnFuelCommandException;
import model.Fuelable;

public class BurnFuelCommand implements Command {
    private Fuelable fuelable;

    public BurnFuelCommand(Fuelable fuelable) {
        this.fuelable = fuelable;
    }

    @Override
    public void execute() {
        try {
            validateFuel();
            burnFuel();
        } catch (Exception e) {
            throw new BurnFuelCommandException(e);
        }
    }

    private void burnFuel() {
        fuelable.setFuel(fuelable.getFuel() - fuelable.getBurnStepAmount());
    }

    private void validateFuel() {
        if (fuelable == null ||
                fuelable.getFuel() < 0 ||
                fuelable.getBurnStepAmount() < 0 ||
        (fuelable.getFuel() - fuelable.getBurnStepAmount() < 0)) {
            throw new IllegalArgumentException();
        }
    }
}
