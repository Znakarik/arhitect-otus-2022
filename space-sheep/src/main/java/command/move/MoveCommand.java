package command.move;

import command.Command;
import exceptions.MoveCommandException;
import model.Movable;

public class MoveCommand implements Command {
    private Movable movable;

    public MoveCommand(Movable movable) {
        this.movable = movable;
    }

    @Override
    public void execute() {
        try {

            validateMovable();
            Position position = movable.getPosition();
            Velocity velocity = movable.getVelocity();
            movable.setPosition(calculatePosition(position, velocity));
        } catch (Exception e) {
            throw new MoveCommandException(e);
        }
    }

    private Position calculatePosition(Position position, Velocity velocity) {
        return new Position(
                position.getX() + velocity.getDx(),
                position.getY() + velocity.getDy());
    }

    private void validateMovable() {
        if (this.movable == null) {
            throw new IllegalArgumentException("movable object cannot be null");
        } else if (movable.getPosition() == null || movable.getVelocity() == null) {
            throw new IllegalArgumentException("position || velocity are illegal");
        }
    }
}
