package command.macro;

import command.Command;
import exceptions.CommandException;

import java.util.Arrays;

public abstract class MacroCommand implements Command {
    private Command[] commands;

    public MacroCommand(Command[] commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        validateCommands();
        try {
            Arrays.stream(commands).forEach(Command::execute);
        } catch (Exception e) {
            throw new CommandException(e);
        }
    }

    private void validateCommands() {
        if (commands == null || commands.length == 0) {
            throw new IllegalArgumentException();
        }
    }
}
