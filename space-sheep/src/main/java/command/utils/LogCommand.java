package command.utils;

import command.Command;

public class LogCommand implements Command {
    private Exception exception;

    public LogCommand(Exception exception) {
        this.exception = exception;
    }

    @Override
    public void execute() {
        System.out.println(this.getClass() + " handler пишет в лог информацию об ошибке : " + exception);
    }
}
