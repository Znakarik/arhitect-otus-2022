package command.utils;

import command.Command;

public class RepeaterCommand implements Command  {
    private Command command;

    public RepeaterCommand(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
    }

    @Override
    public String toString() {
        return "RepeaterCommand{" +
                "command=" + command +
                "hashCode()=" + command.hashCode() +
                '}';
    }
}
