package command.executor;

import command.Command;
import ioc.Ioc;
import ioc.constants.IocConstants;
import org.apache.log4j.Logger;

import java.util.Queue;

public class CommandExecutor implements Runnable, Executor {
    private final Logger LOGGER = Logger.getLogger(CommandExecutor.class);

    private final String scopeId;
    private final String UUID;
    private boolean softStopEnabled;
    private boolean hardStopEnabled;

    public CommandExecutor(String scopeId) {
        this.scopeId = scopeId;
        this.UUID = java.util.UUID.randomUUID().toString();
    }

    @Override
    public void run() {
        boolean isActive = !hardStopEnabled;
        Queue<Command> queue = Ioc.Resolve(IocConstants.CommandsQueue, "scopeId:" + scopeId);
        Command command;
        while (isActive) {
            command = queue.peek();

            if (command != null) {
                try {
                    LOGGER.info(UUID + " | Исполняем команду " + command);
                    command.execute();
                } catch (Exception e) {
                    LOGGER.error("Поймано исключение во время исполнения команды", e);
                    queue.remove(command);
                    continue;
                }
                queue.remove(command);
            }

            if (hardStopEnabled) {
                LOGGER.info("hardStopEnabled. Заканчиваем выполнение");
                isActive = false;
                Thread.currentThread().interrupt();
            }

            if (softStopEnabled && queue.isEmpty()) {
                LOGGER.info("softStopEnabled. Заканчиваем выполнение");
                isActive = false;
                Thread.currentThread().interrupt();
            }

            LOGGER.info(this + " | Команд нет. Засыпаем");
        }
        LOGGER.info(this + " | Заканчиваем выполнение потока");
    }

    @Override
    public void softStop() {
        softStopEnabled = true;
    }

    @Override
    public void hardStop() {
        hardStopEnabled = true;
    }

    @Override
    public String toString() {
        return "CommandExecutor{" +
                "UUID='" + UUID + '\'' +
                '}';
    }
}
