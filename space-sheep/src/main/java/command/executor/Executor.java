package command.executor;

public interface Executor {

    void hardStop();

    void softStop();
}
