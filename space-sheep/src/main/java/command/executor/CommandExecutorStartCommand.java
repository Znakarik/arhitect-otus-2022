package command.executor;

import command.Command;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.enumerations.Methods;
import org.apache.log4j.Logger;

public class CommandExecutorStartCommand implements Command {
    private final Logger LOGGER = Logger.getLogger(CommandExecutor.class);

    private final String scopeId;

    public CommandExecutorStartCommand(String scopeId) {
        this.scopeId = scopeId;
    }

    @Override
    public void execute() {
        LOGGER.info("Создаем исполнителя задач и запускаем. " + IocConstants.scopeIdRegexp + scopeId);
        CommandExecutor commandExecutor = new CommandExecutor(scopeId);
        Ioc.Resolve(Methods.REGISTER.getMethodName(),
                IocConstants.CommandExecutor,
                IocConstants.scopeIdRegexp + scopeId,
                commandExecutor);
        Thread thread = new Thread(commandExecutor);

        Ioc.Resolve(Methods.REGISTER.getMethodName(),
                IocConstants.CommandExecutorThread,
                IocConstants.scopeIdRegexp + scopeId,
                thread);
        thread.start();
    }
}
