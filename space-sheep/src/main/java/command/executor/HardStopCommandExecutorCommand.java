package command.executor;

import command.Command;
import ioc.Ioc;
import ioc.constants.IocConstants;
import org.apache.log4j.Logger;

public class HardStopCommandExecutorCommand implements Command {
    private final Logger LOGGER = Logger.getLogger(HardStopCommandExecutorCommand.class);

    private final String scopeId;

    public HardStopCommandExecutorCommand(String scopeId) {
        this.scopeId = scopeId;
    }

    @Override
    public void execute() {
        Executor executor = Ioc.Resolve(IocConstants.CommandExecutor,
                IocConstants.scopeIdRegexp + scopeId);
        LOGGER.info("Останавливаем CommandExecutor. " + IocConstants.scopeIdRegexp + " = " + scopeId);
        executor.hardStop();
    }
}
