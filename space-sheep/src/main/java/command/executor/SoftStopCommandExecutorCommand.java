package command.executor;

import command.Command;

public class SoftStopCommandExecutorCommand implements Command {
    private final CommandExecutor executor;

    public SoftStopCommandExecutorCommand(CommandExecutor executor) {
        this.executor = executor;
    }

    @Override
    public void execute() {
        System.out.println("мягко выключаем " + executor);
        executor.softStop();
    }
}
