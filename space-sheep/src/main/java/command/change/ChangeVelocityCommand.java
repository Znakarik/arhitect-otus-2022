package command.change;

import command.Command;
import exceptions.ChangeVelocityCommandException;
import model.Movable;
import model.Rotatable;

public class ChangeVelocityCommand implements Command {

    private Rotatable rotatable;

    public ChangeVelocityCommand(Rotatable rotatable) {
        this.rotatable = rotatable;
    }

    @Override
    public void execute() {
        try {
            validateRotatable();
            rotatable.setDirection(calculateNewVelocity());
        } catch (Exception e) {
            throw new ChangeVelocityCommandException(e);
        }
    }

    private int calculateNewVelocity() {
        int direction = rotatable.getDirection();
        int directionsNum = rotatable.getDirectionsNumber();
        return (direction + 1) % directionsNum;

        // todo тут еще нужно изменить velocity, но не поняла откуда брать новые значени
    }

    private void validateRotatable() {
        if (rotatable == null || rotatable.getDirectionsNumber() == 0) {
            throw new IllegalArgumentException();
        }
    }
}
