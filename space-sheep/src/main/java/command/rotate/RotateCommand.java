package command.rotate;

import command.Command;
import exceptions.RotateCommandException;
import model.Rotatable;

public class RotateCommand implements Command {
    private Rotatable rotatable;

    public RotateCommand(Rotatable rotatable) {
        this.rotatable = rotatable;
    }

    @Override
    public void execute() {
        try {
            validateRotatable();
            rotatable.setDirection(calculateDirection());
        } catch (Exception e) {
            throw new RotateCommandException(e);
        }
    }

    private int calculateDirection() {
        return (rotatable.getDirection() + rotatable.getDirection() * rotatable.getAngularVelocity())
                % rotatable.getDirectionsNumber();
    }

    private void validateRotatable() {
        if (this.rotatable == null) {
            throw new IllegalArgumentException("rotatable object cannot be null");
        } else if (this.rotatable.getDirectionsNumber() == 0) {
            throw new IllegalArgumentException("directions number could not be 0");
        }
    }
}
