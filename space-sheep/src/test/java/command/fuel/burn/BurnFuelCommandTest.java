package command.fuel.burn;

import command.Command;
import exceptions.BurnFuelCommandException;
import model.Fuelable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class BurnFuelCommandTest {

    @Test
    void nullFuel() {
        // given
        Command command = new BurnFuelCommand(null);
        // then
        BurnFuelCommandException ex = Assertions.assertThrows(
                BurnFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void invalidGetFuel() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(-1);
        Mockito.when(fuelable.getBurnStepAmount()).thenReturn(10);
        Command command = new BurnFuelCommand(fuelable);
        // then
        BurnFuelCommandException ex = Assertions.assertThrows(
                BurnFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void invalidBurnStep() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getBurnStepAmount()).thenReturn(-1);
        Mockito.when(fuelable.getFuel()).thenReturn(10);
        Command command = new BurnFuelCommand(fuelable);
        // then
        BurnFuelCommandException ex = Assertions.assertThrows(
                BurnFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void burnFuelSuccess() {
        // given
        int currentFuel = 10;
        int burnStep = 10;
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(currentFuel);
        Mockito.when(fuelable.getBurnStepAmount()).thenReturn(burnStep);
        Command command = new BurnFuelCommand(fuelable);
        // when
        Assertions.assertDoesNotThrow(command::execute);
        // then
        ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(fuelable).setFuel(argument.capture());
        Assertions.assertEquals(0, argument.getValue());
    }

    @Test
    void fuelLessBurnStep() {
        // given
        int currentFuel = 10;
        int burnStep = 15;
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(currentFuel);
        Mockito.when(fuelable.getBurnStepAmount()).thenReturn(burnStep);
        Command command = new BurnFuelCommand(fuelable);
        // then
        BurnFuelCommandException ex = Assertions.assertThrows(
                BurnFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }
}
