package command.fuel.check;

import command.Command;
import exceptions.CheckFuelCommandException;
import model.Fuelable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CheckFuelCommandTest {

    @Test
    void enoughFuel() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(20);
        Mockito.when(fuelable.getMinimumFuel()).thenReturn(15);
        Command command = new CheckFuelCommand(fuelable);
        // then
        Assertions.assertDoesNotThrow(command::execute);
    }

    @Test
    void notEnoughFuel() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(10);
        Mockito.when(fuelable.getMinimumFuel()).thenReturn(15);
        Command command = new CheckFuelCommand(fuelable);
        // then
        Assertions.assertThrows(CheckFuelCommandException.class, command::execute);
    }

    @Test
    void nullFuel() {
        // given
        Command command = new CheckFuelCommand(null);
        // then
        CheckFuelCommandException ex = Assertions.assertThrows(
                CheckFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void invalidGetFuel() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(-1);
        Mockito.when(fuelable.getMinimumFuel()).thenReturn(10);
        Command command = new CheckFuelCommand(fuelable);
        // then
        CheckFuelCommandException ex = Assertions.assertThrows(
                CheckFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
        Mockito.verify(fuelable, Mockito.times(1)).getFuel();
    }

    @Test
    void invalidGetMinimumFuel() {
        // given
        Fuelable fuelable = Mockito.mock(Fuelable.class);
        Mockito.when(fuelable.getFuel()).thenReturn(13);
        Mockito.when(fuelable.getMinimumFuel()).thenReturn(-10);
        Command command = new CheckFuelCommand(fuelable);
        // then
        CheckFuelCommandException ex = Assertions.assertThrows(
                CheckFuelCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
        Mockito.verify(fuelable, Mockito.times(1)).getMinimumFuel();
    }
}
