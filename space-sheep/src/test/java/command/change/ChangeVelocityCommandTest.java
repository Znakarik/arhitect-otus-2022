package command.change;

import command.Command;
import command.move.Position;
import exceptions.ChangeVelocityCommandException;
import model.Rotatable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class ChangeVelocityCommandTest {

    @Test
    void nullRotatable() {
        // given
        Command command = new ChangeVelocityCommand(null);
        // then
        ChangeVelocityCommandException ex = Assertions.assertThrows(
                ChangeVelocityCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void directionsNumberZero() {
        // given
        Rotatable rotatable = Mockito.mock(Rotatable.class);
        Mockito.when(rotatable.getDirectionsNumber()).thenReturn(0);

        Command command = new ChangeVelocityCommand(rotatable);

        ChangeVelocityCommandException ex = Assertions.assertThrows(
                ChangeVelocityCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
        Mockito.verify(rotatable, Mockito.times(1)).getDirectionsNumber();
    }

    @Test
    void successChangeDirection() {
        // given
        Rotatable rotatable = Mockito.mock(Rotatable.class);
        Mockito.when(rotatable.getDirectionsNumber()).thenReturn(8);
        Mockito.when(rotatable.getDirection()).thenReturn(0);

        Command command = new ChangeVelocityCommand(rotatable);

        // when
        Assertions.assertDoesNotThrow(command::execute);
        // then
        ArgumentCaptor<Integer> newDirectionArgument = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(rotatable).setDirection(newDirectionArgument.capture());
        Assertions.assertEquals(1, newDirectionArgument.getValue());
    }

    // todo написать тесты на изменение velocity
}
