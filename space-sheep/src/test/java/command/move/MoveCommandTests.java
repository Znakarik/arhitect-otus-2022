package command.move;

import command.Command;
import exceptions.MoveCommandException;
import model.Movable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class MoveCommandTests {

    /**
     * Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3)
     * движение меняет положение объекта на (5, 8)
     */
    @Test
    void moveSuccess() {
        // given
        Movable movable = Mockito.spy(Movable.class);
        Position position = new Position(12, 5);
        Velocity velocity = new Velocity(-7, 3);
        Mockito.when(movable.getPosition()).thenReturn(position);
        Mockito.when(movable.getVelocity()).thenReturn(velocity);

        Command command = new MoveCommand(movable);
        // when
        command.execute();
        // then
        ArgumentCaptor<Position> positionArgument = ArgumentCaptor.forClass(Position.class);
        Mockito.verify(movable).setPosition(positionArgument.capture());
        Assertions.assertEquals(5, positionArgument.getValue().getX());
        Assertions.assertEquals(8, positionArgument.getValue().getY());
    }

    /**
     * Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке
     */
    @Test
    void couldNotReadPosition() {
        // given
        Movable movable = Mockito.mock(Movable.class);
        Mockito.when(movable.getVelocity()).thenReturn(new Velocity(0, 0));
        Command command = new MoveCommand(movable);
        // then
        MoveCommandException ex = Assertions.assertThrows(
                MoveCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    /**
     * Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке
     */
    @Test
    void couldNotReadVelocity() {
        // given
        Movable movable = Mockito.mock(Movable.class);
        Mockito.when(movable.getPosition()).thenReturn(new Position(0, 0));
        Command command = new MoveCommand(movable);
        // then
        MoveCommandException ex = Assertions.assertThrows(
                MoveCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void positionAndVelocityNull() {
        Movable movable = Mockito.mock(Movable.class);
        Command command = new MoveCommand(movable);

        MoveCommandException ex = Assertions.assertThrows(
                MoveCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    void nullMovable() {
        Command command = new MoveCommand(null);

        MoveCommandException ex = Assertions.assertThrows(
                MoveCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }
}
