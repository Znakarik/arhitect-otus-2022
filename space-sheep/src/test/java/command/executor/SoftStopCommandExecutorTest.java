package command.executor;

import command.Command;
import command.fuel.check.CheckFuelCommand;
import command.utils.LogCommand;
import command.utils.RepeaterCommand;
import ioc.Game;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import model.Fuelable;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


class SoftStopCommandExecutorTest {

    @Test
    public void softStopSuccess() throws InterruptedException {
        Game firstGame = new Game();
        Queue<Command> firstCommandQueue = prepareCommandsQue();
        firstGame.getContainer().addNew("Commands.Queue", firstCommandQueue);
        String firstScopeId = "firstScopeId";

        Game secondGame = new Game();
        Queue<Command> secondCommandQueue = prepareCommandsQue();
        secondGame.getContainer().addNew("Commands.Queue", secondCommandQueue);
        String secondScopeId = "secondScopeId";

        GameDataRepository<Game> gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer(firstScopeId, firstGame);
        gameDataRepository.addNewContainer(secondScopeId, secondGame);

        Ioc.loadDataRepository(gameDataRepository);

        CommandExecutorStartCommand command = new CommandExecutorStartCommand(firstScopeId);
        CommandExecutorStartCommand commandSecond = new CommandExecutorStartCommand(secondScopeId);
        command.execute();
        commandSecond.execute();

        CommandExecutor firstExecutor = Ioc.Resolve(IocConstants.CommandExecutor, IocConstants.scopeIdRegexp + firstScopeId);
        CommandExecutor secondExecutor = Ioc.Resolve(IocConstants.CommandExecutor, IocConstants.scopeIdRegexp + secondScopeId);

        SoftStopCommandExecutorCommand firstStop = new SoftStopCommandExecutorCommand(firstExecutor);
        firstStop.execute();
        SoftStopCommandExecutorCommand secStop = new SoftStopCommandExecutorCommand(secondExecutor);
        secStop.execute();

        Thread firstExecutorThread = Ioc.Resolve(IocConstants.CommandExecutorThread, IocConstants.scopeIdRegexp + firstScopeId);
        Thread secondExecutorThread = Ioc.Resolve(IocConstants.CommandExecutorThread, IocConstants.scopeIdRegexp + secondScopeId);
        firstExecutorThread.join();
        secondExecutorThread.join();

        firstCommandQueue.forEach(cmd -> Mockito.verify(cmd).execute());
        secondCommandQueue.forEach(cmd -> Mockito.verify(cmd).execute());
    }

    private Queue<Command> prepareCommandsQue() {
        Queue<Command> commandQueue = new ConcurrentLinkedQueue<>();
        commandQueue.add(Mockito.mock(RepeaterCommand.class));
        commandQueue.add(Mockito.mock(CheckFuelCommand.class));
        commandQueue.add(Mockito.spy(new RepeaterCommand(Mockito.mock(Command.class))));
        commandQueue.add(Mockito.spy(new RepeaterCommand(Mockito.mock(Command.class))));
        commandQueue.add(Mockito.spy(new RepeaterCommand(Mockito.mock(Command.class))));
        commandQueue.add(Mockito.spy(new LogCommand(new RuntimeException())));
        commandQueue.add(Mockito.spy(new LogCommand(new RuntimeException())));
        commandQueue.add(Mockito.spy(new LogCommand(new RuntimeException())));
        commandQueue.add(Mockito.spy(new LogCommand(new RuntimeException())));
        commandQueue.add(Mockito.spy(new LogCommand(new RuntimeException())));
        commandQueue.add(Mockito.spy(new CheckFuelCommand(Mockito.mock(Fuelable.class))));
        commandQueue.add(Mockito.spy(new CheckFuelCommand(Mockito.mock(Fuelable.class))));
        commandQueue.add(Mockito.spy(new CheckFuelCommand(Mockito.mock(Fuelable.class))));
        commandQueue.add(Mockito.spy(new CheckFuelCommand(Mockito.mock(Fuelable.class))));
        commandQueue.add(Mockito.spy(new CheckFuelCommand(Mockito.mock(Fuelable.class))));
        return commandQueue;
    }

}