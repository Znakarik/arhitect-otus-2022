package command.executor;

import command.Command;
import ioc.Game;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.Queue;

class CommandExecutorTest {

    @Test
    public void hardStop() throws InterruptedException {

        String scopeId = "scopeId";

        Game firstGame = new Game();

        Command command = Mockito.mock(Command.class);
        Command command2 = Mockito.mock(Command.class);
        Command command3 = Mockito.mock(Command.class);

        Queue<Command> commandQueue = new LinkedList<>();
        commandQueue.add(command);
        commandQueue.add(command2);
        commandQueue.add(command3);

        firstGame.getContainer().addNew(
                IocConstants.CommandsQueue,
                IocConstants.scopeIdRegexp + scopeId,
                commandQueue);

        GameDataRepository<Game> gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer(scopeId, firstGame);
        Ioc.loadDataRepository(gameDataRepository);
        CommandExecutor underTest = new CommandExecutor("scopeId");
        // when
        Thread executor = new Thread(underTest);
        Thread stopper = new Thread(underTest::hardStop);

        executor.start();
        Thread.sleep(200);
        stopper.start();

       // Thread.currentThread().join();
        Mockito.verify(command).execute();
        Mockito.verify(command2).execute();
        Mockito.verify(command3).execute();

    }

    @Test
    public void softStop() throws InterruptedException {

        String scopeId = "scopeId";

        Game firstGame = new Game();

        Command command = Mockito.mock(Command.class);
        Command command2 = Mockito.mock(Command.class);
        Command command3 = Mockito.mock(Command.class);

        Queue<Command> commandQueue = new LinkedList<>();
        commandQueue.add(command);
        commandQueue.add(command2);
        commandQueue.add(command3);

        firstGame.getContainer().addNew(
                IocConstants.CommandsQueue,
                IocConstants.scopeIdRegexp + scopeId,
                commandQueue);

        GameDataRepository<Game> gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer(scopeId, firstGame);
        Ioc.loadDataRepository(gameDataRepository);
        CommandExecutor underTest = new CommandExecutor("scopeId");
        // when
        Thread executor = new Thread(underTest);
        Thread stopper = new Thread(underTest::softStop);

        executor.start();
        Thread.sleep(100);
        stopper.start();

        Mockito.verify(command).execute();
        Mockito.verify(command2).execute();
        Mockito.verify(command3).execute();
    }

    @Test
    public void throwsEx() throws InterruptedException {

        String scopeId = "scopeId";

        Game firstGame = new Game();

        Command command = Mockito.mock(Command.class);
        Command command2 = Mockito.mock(Command.class);
        Command command3 = Mockito.mock(Command.class);
        Mockito.doThrow(new RuntimeException()).when(command).execute();

        Queue<Command> commandQueue = new LinkedList<>();
        commandQueue.add(command);
        commandQueue.add(command2);
        commandQueue.add(command3);

        firstGame.getContainer().addNew(
                IocConstants.CommandsQueue,
                IocConstants.scopeIdRegexp + scopeId,
                commandQueue);

        GameDataRepository<Game> gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer(scopeId, firstGame);
        Ioc.loadDataRepository(gameDataRepository);
        CommandExecutor underTest = new CommandExecutor("scopeId");
        // when
        Thread executor = new Thread(underTest);

        executor.start();
        Thread.sleep(1_000);
        underTest.hardStop();

        Mockito.verify(command).execute();
        Mockito.verify(command2).execute();
        Mockito.verify(command3).execute();
    }
}