package command.executor;

import ioc.Game;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class HardStopCommandExecutorCommandTest {

    @Test
    public void stopExecutor() {
        String scopeId = "scopeId";
        HardStopCommandExecutorCommand command = new HardStopCommandExecutorCommand(scopeId);

        Executor executor = Mockito.mock(Executor.class);
        Game firstGame = new Game();

        firstGame.getContainer().addNew(
                IocConstants.CommandExecutor,
                IocConstants.scopeIdRegexp + scopeId,
                executor);

        GameDataRepository<Game> gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer(scopeId, firstGame);
        Ioc.loadDataRepository(gameDataRepository);

        command.execute();

        Mockito.verify(executor).hardStop();
    }

}