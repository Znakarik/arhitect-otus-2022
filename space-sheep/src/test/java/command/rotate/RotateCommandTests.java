package command.rotate;

import command.Command;
import exceptions.RotateCommandException;
import model.Rotatable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class RotateCommandTests {

    @Test
    public void successRotate() {
        // given
        Rotatable rotatable = Mockito.mock(Rotatable.class);
        Mockito.when(rotatable.getDirection()).thenReturn(1);
        Mockito.when(rotatable.getAngularVelocity()).thenReturn(10);
        Mockito.when(rotatable.getDirectionsNumber()).thenReturn(8);
        // when
        Command command = new RotateCommand(rotatable);
        command.execute();
        // then
        ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(rotatable).setDirection(argument.capture());
        Assertions.assertEquals(3, argument.getValue());
    }

    @Test
    public void nullRotatable() {
        // given
        Command command = new RotateCommand(null);
        // then
        RotateCommandException ex = Assertions.assertThrows(
                RotateCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);    }

    @Test
    public void directionsNumberZero() {
        // given
        Rotatable rotatable = Mockito.mock(Rotatable.class);
        Mockito.when(rotatable.getDirection()).thenReturn(1);
        Mockito.when(rotatable.getAngularVelocity()).thenReturn(10);
        Mockito.when(rotatable.getDirectionsNumber()).thenReturn(0);
        Command command = new RotateCommand(rotatable);
        // then
        RotateCommandException ex = Assertions.assertThrows(
                RotateCommandException.class,
                command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }
}
