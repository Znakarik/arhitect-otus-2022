package command.macro;

import command.Command;
import exceptions.BurnFuelCommandException;
import exceptions.CheckFuelCommandException;
import exceptions.CommandException;
import exceptions.MoveCommandException;
import model.Fuelable;
import command.fuel.burn.BurnFuelCommand;
import command.fuel.check.CheckFuelCommand;
import model.Movable;
import command.move.MoveCommand;
import command.move.Position;
import command.move.Velocity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class StraightMoveCommandTest {

    /**
     * Движение по прямой включает в себя
     * - move
     * - checkFuel
     * - burn
     */
    @Test
    public void successStraight() {
        // given
        Sheep sheep = Mockito.mock(Sheep.class);
        mockSuccessMovable(sheep);
        mockSuccessFuelable(sheep);

        Command moveCommand = new MoveCommand(sheep);
        Command checkFuelCommand = new CheckFuelCommand(sheep);
        Command burnFuelCommand = new BurnFuelCommand(sheep);

        Command[] commands = new Command[]{moveCommand, checkFuelCommand, burnFuelCommand};
        MacroCommand macroCommand = new MacroCommand(commands) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // then
        Assertions.assertDoesNotThrow(macroCommand::execute);
    }

    @Test
    void nullCommands() {
        // given
        Command macroCommand = new MacroCommand(null) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // then
        Assertions.assertThrows(IllegalArgumentException.class, macroCommand::execute);
    }

    @Test
    void illegalMovableThrows() {
        // given
        Movable movable = Mockito.mock(Movable.class);
        Command moveCommand = Mockito.spy(new MoveCommand(movable));

        Command[] commands = new Command[]{moveCommand};
        Command straightMoveCommand = new MacroCommand(commands) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // when
        CommandException ex = Assertions.assertThrows(CommandException.class, straightMoveCommand::execute);
        // then
        Assertions.assertTrue(ex.getCause() instanceof MoveCommandException);
        Assertions.assertTrue(ex.getCause().getCause() instanceof IllegalArgumentException);
        Mockito.verify(moveCommand, Mockito.times(1)).execute();
    }

    @Test
    void checkBurnFuelThrows() {
        // given
        Sheep sheep = Mockito.mock(Sheep.class);
        mockSuccessMovable(sheep);
        mockSuccessFuelable(sheep);

        Command moveCommand = new MoveCommand(sheep);
        Command checkFuelCommand = new CheckFuelCommand(null);
        Command burnFuelCommand = new BurnFuelCommand(sheep);

        Command[] commands = new Command[]{moveCommand, checkFuelCommand, burnFuelCommand};
        MacroCommand macroCommand = new MacroCommand(commands) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // then
        CommandException ex = Assertions.assertThrows(
                CommandException.class,
                macroCommand::execute);
        Assertions.assertTrue(ex.getCause() instanceof CheckFuelCommandException);
    }

    @Test
    void moveThrows() {
        // given
        Sheep sheep = Mockito.mock(Sheep.class);
        mockSuccessMovable(sheep);
        mockSuccessFuelable(sheep);

        Command moveCommand = new MoveCommand(null);
        Command checkFuelCommand = new CheckFuelCommand(sheep);
        Command burnFuelCommand = new BurnFuelCommand(sheep);

        Command[] commands = new Command[]{moveCommand, checkFuelCommand, burnFuelCommand};
        MacroCommand macroCommand = new MacroCommand(commands) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // then
        CommandException ex = Assertions.assertThrows(
                CommandException.class,
                macroCommand::execute);
        Assertions.assertTrue(ex.getCause() instanceof MoveCommandException);
    }

    @Test
    void burnFuelThrows() {
        // given
        Sheep sheep = Mockito.mock(Sheep.class);
        mockSuccessMovable(sheep);
        mockSuccessFuelable(sheep);

        Command moveCommand = new MoveCommand(sheep);
        Command checkFuelCommand = new CheckFuelCommand(sheep);
        Command burnFuelCommand = new BurnFuelCommand(null);

        Command[] commands = new Command[]{moveCommand, checkFuelCommand, burnFuelCommand};
        MacroCommand macroCommand = new MacroCommand(commands) {
            @Override
            public void execute() {
                super.execute();
            }
        };
        // then
        CommandException ex = Assertions.assertThrows(
                CommandException.class,
                macroCommand::execute);
        Assertions.assertTrue(ex.getCause() instanceof BurnFuelCommandException);
    }

    abstract class Sheep implements Movable, Fuelable {

    }

    private void mockSuccessMovable(Movable movable) {
        Position position = new Position(12, 5);
        Velocity velocity = new Velocity(-7, 3);
        Mockito.when(movable.getPosition()).thenReturn(position);
        Mockito.when(movable.getVelocity()).thenReturn(velocity);
    }

    private void mockSuccessFuelable(Fuelable fuelable) {
        // check
        Mockito.when(fuelable.getFuel()).thenReturn(20);
        Mockito.when(fuelable.getMinimumFuel()).thenReturn(15);
        // burn
        Mockito.when(fuelable.getBurnStepAmount()).thenReturn(10);
    }
}
