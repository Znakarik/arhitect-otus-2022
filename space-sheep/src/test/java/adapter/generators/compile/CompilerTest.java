package adapter.generators.compile;

import org.junit.jupiter.api.Test;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

class CompilerTest {

//    @Test
    public  void tryToCompile() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
        // Prepare source somehow.
        String source = "package generated; public class Testt { static { System.out.println(\"hello\"); } public Testt() { System.out.println(\"world\"); } }";

        // Save source in .java file.
        File root = new File("/Users/znakarik/IdeaProjects/arhitect-otus-2022/space-sheep/src/test/java/generated"); // On Windows running on C:\, this is C:\java.
        File sourceFile = new File(root, "Testt.java");
        sourceFile.getParentFile().mkdirs();
        Files.write(sourceFile.toPath(), source.getBytes(StandardCharsets.UTF_8));

        // Compile source file.
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, sourceFile.getPath());

        // Load and instantiate compiled class.
        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
        Class<?> cls = Class.forName("generated.Testt", true, classLoader); // Should print "hello".
        Object instance = cls.newInstance(); // Should print "world".
        System.out.println(instance); // Should print "test.Test@hashcode".
    }

    @Test
    public void tryYoCreateFile() throws IOException {
        Path tempFile = Files.createTempFile("", "");
        tempFile.toFile().deleteOnExit();
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write("==============\n*******Hello\n==============");
        fileWriter.close();

        FileReader fileReader = new FileReader(tempFile.toFile());
        Scanner scanner = new Scanner(fileReader);

        while (scanner.hasNextLine()) {
            System.out.println(scanner.nextLine());
        }

    }
}