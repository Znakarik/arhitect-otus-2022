package adapter.generators.classparts;

import model.Movable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class SimpleClassPartsGeneratorTest {

    @Test
    void generateImport() {
        String result = SimpleClassPartsGenerator.generateImport(Movable.class);
        System.out.println(result);
        String expected = "import model.Movable;";
        Assertions.assertEquals(expected, result);
    }

    @Test
    void generateImportsFromInterface() {
        String strResult = SimpleClassPartsGenerator.generateImportsFromInterface(Movable.class).trim();
        List<String> result = Arrays.asList(strResult.split(";"));
        result = result.stream().map(s -> s.replaceAll("\n", "")).collect(Collectors.toList());
        Collections.sort(result);
        System.out.println(result);
        List<String> expected = Arrays.asList(
                "import command.move.Velocity",
                "import command.move.Position",
                "import command.move.Position");
        Collections.sort(expected);

        Assertions.assertTrue(result.containsAll(expected));
    }
}