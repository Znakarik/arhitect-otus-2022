package adapter.generators;

import adapter.generators.classparts.SetterGeneratorWithIoc;
import command.move.Position;
import ioc.constants.IocConstants;
import ioc.Ioc;
import model.Movable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

class SetterGeneratorWithIocTest {

    private MockedStatic<Ioc> ioc;

    @BeforeEach
    public void before() {
        MockitoAnnotations.openMocks(this);
        ioc = Mockito.mockStatic(Ioc.class);
    }

    @AfterEach
    public void after() {
        ioc.close();
    }

    @Test
    public void success() throws NoSuchMethodException {
        Class interfaceForGen = Movable.class;
        String targetClassName = "Tank";
        String uobjectFieldName = "uobject";

        // получаем из ioc имя генерящегося класса
        String getGeneratedClassNameIocCode = String.format(IocConstants.generatedClassName, interfaceForGen.getSimpleName());
        when(Ioc.Resolve(Mockito.eq(getGeneratedClassNameIocCode))).thenReturn(targetClassName);

        // получаем из ioc имя переменной uobject
        String getUObjectFieldNameIocCode = String.format(IocConstants.uobjectFieldName, interfaceForGen.getSimpleName());
        when(Ioc.Resolve(getUObjectFieldNameIocCode)).thenReturn(uobjectFieldName);

        MethodGenerator underTest = new SetterGeneratorWithIoc(
                interfaceForGen.getDeclaredMethod("setPosition", Position.class),
                interfaceForGen
        );

        String output = underTest.generate();
        System.out.println(output);

        String expected =
                "public void setPosition(Position position){\n" +
                "Ioc.Resolve(\"Tank.Operations.Movable:position.set\", uobject, position);\n" +
                "}";
        Assertions.assertEquals(expected, output.trim());
    }

}