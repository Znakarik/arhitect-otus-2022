package adapter;

import adapter.generators.IocObjectAware;
import adapter.generators.SimpleUObject;
import adapter.generators.UObject;
import adapter.generators.classes.GenerateWithStrategyFromIoc;
import adapter.generators.classes.GeneratorWithIoc;
import adapter.generators.classes.InmemoryClassGeneratorWithIoc;
import adapter.generators.classes.SimpleGenerator;
import adapter.generators.compile.FilesCompiler;
import command.move.Position;
import command.move.Velocity;
import forgen.IPojo;
import forgen.SimpleInterface;
import ioc.Container;
import ioc.Game;
import ioc.Ioc;
import ioc.constants.IocConstants;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import model.Movable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.function.Function;

class ClassGeneratorTest {
    private GameDataRepository<Game> gameDataRepository;
    private Game game;

    @BeforeEach
    public void before() {
        game = new Game();
        Game threadLocal = new Game();
        gameDataRepository = new GameDataRepositoryImpl();
        gameDataRepository.addNewContainer("1234", threadLocal);
        gameDataRepository = Ioc.loadDataRepository(gameDataRepository);
    }

    @Test
    @Disabled // todo перевести на inmemory
    public void generateWithMethods() throws IOException {
        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        SimpleGenerator generator =
                new SimpleGenerator(pwd + classPath + IPojo.class.getSimpleName() + ".java", IPojo.class);
        generator.generate();
    }

    @Test
    @Disabled // todo перевести на inmemory
    public void generateWithoutMethods() throws IOException {
        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        SimpleGenerator generator =
                new SimpleGenerator(pwd + classPath + SimpleInterface.class.getSimpleName() + ".java", SimpleInterface.class);
        generator.generate();
    }

    @Test
    @Disabled // todo перевести на inmemory
    public void generateWithUObjectFromIoc() throws IOException {
        gameDataRepository.getCurrent().getContainer().addNew(IPojo.class.getSimpleName() + ".UObject.type", Object.class);
        gameDataRepository.getCurrent().getContainer().addNew("IPojo.Generation.ClassName", "IPojoImpl");
        gameDataRepository.getCurrent().getContainer().addNew("IPojo.UObject.field.name", "object");
        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        Generator generator =
                new GeneratorWithIoc(
                        pwd + classPath + IPojo.class.getSimpleName() + "Impl.java",
                        IPojo.class,
                        new FilesCompiler());
        generator.generate();
    }

    @Test
    @Disabled // todo перевести на inmemory
    public void generateCompileAndGetInstance() throws Throwable {
        gameDataRepository.getCurrent().getContainer().addNew("IPojo.Generation.ClassName", "IPojoImpl");
        gameDataRepository.getCurrent().getContainer().addNew("IPojo.UObject.field.name", "object");
        IocObjectAware iocObjectAware = new IocObjectAware(game, IPojo.class, new Object());
        iocObjectAware.apply();
        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        Generator generator =
                new GeneratorWithIoc(
                        pwd + classPath + IPojo.class.getSimpleName() + "Impl.java",
                        IPojo.class,
                        new FilesCompiler());
        generator.generate();

        IPojo pojo = Ioc.Resolve("Adapter", IPojo.class);
        Assertions.assertNotNull(pojo.getObject());
    }

    @Test
    @Disabled // todo перевести на inmemory
    public void generateWithStrategyFromIoc() throws IOException {
        Container currentContainer = gameDataRepository.getCurrent().getContainer();

        Class toGen = Movable.class;

        String targetClassName = "Tank";

        // подготавливаем бин UObject
        SimpleUObject uObject = new SimpleUObject();
        Position startPosition = new Position(1, 1);
        uObject.addNewProp("position", startPosition);
        uObject.addNewProp("velocity", new Velocity(10, 2));
        // кладем UObject в IOC
        currentContainer.addNew(
                String.format(IocConstants.uobjectTypePattern, toGen.getSimpleName()),
                uObject.getClass());

        currentContainer.addNew(
                String.format(IocConstants.uobjectValuePattern, toGen.getSimpleName()),
                uObject);

        currentContainer.addNew(
                String.format(IocConstants.generatedClassName, toGen.getSimpleName()),
                targetClassName
        );

        Function<SimpleUObject, Position> getPositionFunction = (obj) -> (Position) obj.getProp("position");
        currentContainer.addNew("Tank.Operations.Movable:position.get", getPositionFunction);
        Function<SimpleUObject, Velocity> getVelocityFunction = (obj) -> (Velocity) obj.getProp("velocity");
        currentContainer.addNew("Tank.Operations.Movable:velocity.get", getVelocityFunction);
        Position newPosition = new Position(10, 20);
        currentContainer.addNew("Tank.Operations.Movable:position.set", newPosition);
        currentContainer.addNew(
                String.format(IocConstants.uobjectFieldName, toGen.getSimpleName()),
                "uobject"
        );

        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        Generator generator =
                new GenerateWithStrategyFromIoc(
                        pwd + classPath + targetClassName + ".java",
                        toGen,
                        new FilesCompiler());
        generator.generate();

        Movable movable = Ioc.Resolve("Adapter", Movable.class);
        Assertions.assertNotNull(movable);

        Assertions.assertNotNull(movable.getPosition());
        Assertions.assertTrue(movable.getPosition() instanceof Position);

        Assertions.assertNotNull(movable.getVelocity());
        Assertions.assertTrue(movable.getVelocity() instanceof Velocity);

        // проверяем перед тем как зададим новое значение
        Assertions.assertEquals(startPosition.getX(), movable.getPosition().getX());
        Assertions.assertEquals(startPosition.getY(), movable.getPosition().getY());

        movable.setPosition(newPosition);
        Assertions.assertEquals(newPosition.getX(), movable.getPosition().getX());
        Assertions.assertEquals(newPosition.getY(), movable.getPosition().getY());
    }

    /**
     * - Генерация адаптера
     * - создание его обьекта типа var adapter = IoC.Resolve("Adapter", typeof(IMovable), obj);
     *
     * @throws IOException
     */
    @Test
    @Disabled // todo перевести на inmemory
    public void generatedAdapterWithNewUObject() throws IOException {
        Container currentContainer = gameDataRepository.getCurrent().getContainer();

        Class toGen = Movable.class;

        String targetClassName = "Tank";

        // подготавливаем бин UObject
        SimpleUObject uObject = new SimpleUObject();
        Position startPosition = new Position(1, 1);
        uObject.addNewProp("position", startPosition);
        uObject.addNewProp("velocity", new Velocity(10, 2));
        // кладем UObject в IOC
        currentContainer.addNew(
                String.format(IocConstants.uobjectTypePattern, toGen.getSimpleName()),
                uObject.getClass());

        currentContainer.addNew(
                String.format(IocConstants.uobjectValuePattern, toGen.getSimpleName()),
                uObject);

        currentContainer.addNew(
                String.format(IocConstants.generatedClassName, toGen.getSimpleName()),
                targetClassName
        );

        Function<SimpleUObject, Position> getPositionFunction = (obj) -> (Position) obj.getProp("position");
        currentContainer.addNew("Tank.Operations.Movable:position.get", getPositionFunction);

        currentContainer.addNew(
                String.format(IocConstants.uobjectFieldName, toGen.getSimpleName()),
                "uobject"
        );

        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        Generator generator =
                new GenerateWithStrategyFromIoc(
                        pwd + classPath + targetClassName + ".java",
                        toGen,
                        new FilesCompiler());
        generator.generate();

        UObject newUObject = new SimpleUObject();
        Position newPosition = new Position(100, 100);
        uObject.addNewProp("position", newPosition);

        Movable movable = Ioc.Resolve("Adapter", Movable.class, newUObject);
        Assertions.assertNotNull(movable);

        Assertions.assertNotNull(movable.getPosition());
        Assertions.assertTrue(movable.getPosition() instanceof Position);
        Assertions.assertEquals(newPosition.getX(), movable.getPosition().getX());
        Assertions.assertEquals(newPosition.getY(), movable.getPosition().getY());
    }

    /**
     * - Генерация адаптера
     * - создание его обьекта типа var adapter = IoC.Resolve("Adapter", typeof(IMovable), obj);
     *
     * @throws IOException
     */
    @Test
    @Disabled
    public void generatedInmemoryAdapterWithNewUObject() throws IOException {
        Container currentContainer = gameDataRepository.getCurrent().getContainer();

        Class toGen = Movable.class;

        String targetClassName = "Tank";

        // подготавливаем бин UObject
        SimpleUObject uObject = new SimpleUObject();
        Position startPosition = new Position(1, 1);
        uObject.addNewProp("position", startPosition);
        uObject.addNewProp("velocity", new Velocity(10, 2));
        // кладем UObject в IOC
        currentContainer.addNew(
                String.format(IocConstants.uobjectTypePattern, toGen.getSimpleName()),
                uObject.getClass());

        currentContainer.addNew(
                String.format(IocConstants.uobjectValuePattern, toGen.getSimpleName()),
                uObject);

        currentContainer.addNew(
                String.format(IocConstants.generatedClassName, toGen.getSimpleName()),
                targetClassName
        );

        Function<SimpleUObject, Position> getPositionFunction = (obj) -> (Position) obj.getProp("position");
        currentContainer.addNew("Tank.Operations.Movable:position.get", getPositionFunction);

        currentContainer.addNew(
                String.format(IocConstants.uobjectFieldName, toGen.getSimpleName()),
                "uobject"
        );

        String pwd = System.getProperty("user.dir");
        String classPath = "/src/main/java/generated/adapters/";

        Generator generator =
                new InmemoryClassGeneratorWithIoc(
                        pwd + classPath + targetClassName + ".java",
                        toGen);
        generator.generate();

        UObject newUObject = new SimpleUObject();
        Position newPosition = new Position(100, 100);
        uObject.addNewProp("position", newPosition);

        Movable movable = Ioc.Resolve("Adapter", Movable.class, newUObject);
        Assertions.assertNotNull(movable);

        Assertions.assertNotNull(movable.getPosition());
        Assertions.assertTrue(movable.getPosition() instanceof Position);
        Assertions.assertEquals(newPosition.getX(), movable.getPosition().getX());
        Assertions.assertEquals(newPosition.getY(), movable.getPosition().getY());
    }
}