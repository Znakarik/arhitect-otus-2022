package exceptions.handlers;

import command.Command;
import command.utils.LogCommand;

import ioc.Ioc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

class TryToRepeatIfThrowsLogHandlerTest {

    private MockedStatic<Ioc> ioc;

    @BeforeEach
    public void before() {
        ioc = Mockito.mockStatic(Ioc.class);
    }

    @AfterEach
    public void after() {
        ioc.close();
    }

    @Test
    public void oneRepeat() {
        // given
        Command command = Mockito.mock(Command.class);
        Exception ex = new RuntimeException();
        Mockito.doThrow(ex).when(command).execute();
        ioc.when(Ioc.Resolve(Mockito.anyString(), Mockito.any()))
                .thenReturn(new LogCommand(ex));
        Command handler = new TryToRepeatIfThrowsLogHandler(command, 1);
        // when
        handler.execute();
        // then
        Mockito.verify(command, Mockito.times(1)).execute();
    }

    @Test
    public void twiceRepeat() {
        // given
        Command command = Mockito.mock(Command.class);
        Exception ex = new RuntimeException();
        Mockito.doThrow(ex).when(command).execute();
        ioc.when(Ioc.Resolve(Mockito.anyString(), Mockito.any()))
                .thenReturn(new LogCommand(ex));
        Command handler = new TryToRepeatIfThrowsLogHandler(command, 2);
        // when
        handler.execute();
        // then
        Mockito.verify(command, Mockito.times(2)).execute();
    }
}