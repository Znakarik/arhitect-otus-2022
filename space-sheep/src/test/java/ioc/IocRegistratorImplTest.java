package ioc;

import command.Command;
import command.fuel.burn.BurnFuelCommand;
import ioc.utils.IocRegistrator;
import ioc.utils.IocRegistratorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IocRegistratorImplTest {

    @Test
    public void register() {
        // given
        Game game = new Game();
        IocRegistrator underTest = new IocRegistratorImpl(game);
        Command command = new BurnFuelCommand(null);

        // when
        underTest.register("command", command);
        // then
        Object commandFromContainer = ((Object[]) game.getContainer().getContainerCopy().get("command"))[0];
        Assertions.assertTrue(commandFromContainer instanceof Command);
        Assertions.assertEquals(command, commandFromContainer);
    }

}