package ioc;

import command.Command;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class ContainerTest {

    @Test
    public void addNewNotNul() {
        Container container = new Container();
        Command command = Mockito.mock(Command.class);
        Object returnedObj = container.addNew("command", command);

        Assertions.assertNotNull(returnedObj);
        Assertions.assertEquals(command, returnedObj);
    }
}