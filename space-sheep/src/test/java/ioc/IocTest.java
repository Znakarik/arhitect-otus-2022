package ioc;

import command.Command;
import command.change.ChangeVelocityCommand;
import exceptions.ChangeVelocityCommandException;
import exceptions.IocNotFoundDependencyException;
import exceptions.NotFoundScopeException;
import ioc.data.GameDataRepository;
import ioc.data.GameDataRepositoryImpl;
import model.Rotatable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.function.Function;

class IocTest {

    private String scopeId = "3232343433";

    private GameDataRepository<Game> dataRepository;

    @BeforeEach
    public void initEnv() {
        Game game = new Game();
        Game threadLocal = new Game();
        dataRepository = new GameDataRepositoryImpl();
        dataRepository.addNewContainer(scopeId, threadLocal);
        dataRepository = Ioc.loadDataRepository(dataRepository);
    }

    @Test
    void registerNewCommandStrategy() {
        // given
        Object commandStrategy = Ioc.Resolve(
                "IoC.Register",
                "changeVelocityCommand",
                (Function<Rotatable, Command>) ChangeVelocityCommand::new);
        // when
        Assertions.assertNotNull(commandStrategy);
        Assertions.assertTrue(commandStrategy instanceof Function);
        // then
        Command command = ((Function<Rotatable, Command>) commandStrategy).apply(Mockito.mock(Rotatable.class));
        Assertions.assertNotNull(command);
        // не подсунули все правильные параметры
        ChangeVelocityCommandException ex = Assertions.assertThrows(ChangeVelocityCommandException.class, command::execute);
        Assertions.assertTrue(ex.getCause() instanceof IllegalArgumentException);
    }

    @Test
    public void createNewScope() {
        dataRepository.clear();

        Ioc.Resolve("Scopes.New", "scopeId");

        Game container = dataRepository.getContainerById("scopeId");
        Assertions.assertNotNull(container);
    }

    @Test
    public void setNotExistingScopeCurrent() {
        dataRepository.getCurrent();

        Assertions.assertThrows(
                NotFoundScopeException.class,
                ()-> Ioc.Resolve("Scopes.Current", "unknown"));
    }

    @Test
    public void setCurrentScopeSuccess() {
        String newCurrentScopeId = "newScope";
        Game threadLocal = new Game();
        dataRepository.addNewContainer(newCurrentScopeId, threadLocal);

        Assertions.assertDoesNotThrow(() -> dataRepository.setCurrentById(newCurrentScopeId));
        Assertions.assertEquals(threadLocal, dataRepository.getCurrent());
    }

    @Test
    public void notFoundDependency() {
        Assertions.assertThrows(IocNotFoundDependencyException.class,
                () -> Ioc.Resolve("Unknown"));
    }
}